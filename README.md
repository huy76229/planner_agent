## Requirements
* Python 3

## Requirement Package
* [dbot_msgs](https://gitlab.com/dogong/dbot_foundation/-/tree/master/dbot_msgs)

## Install Packages
```
python3 -m pip install --no-cache-dir -r setup/requirements.txt
```
## Or Install Packages and Setup Service(Auto)

```
sudo ./setup/setup.sh
```
## Launch The Agent Node(Manual)
```
roslaunch planner_agent planner_agent.launch
```