#!/bin/bash

export PYTHONPATH=/home/nvidia/catkin_ws/devel/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages:/home/nvidia/.local/lib/python3.6/site-packages:/home/nvidia/.local/lib/python3.6/site-packages:/home/nvidia/.local/lib/python3.6/site-packages/python3/dist-packages:/usr/lib/python3.6/dist-packages


export ROS_MASTER_URI=http://192.168.50.68:11311/
export ROS_IP=192.168.50.78


/bin/sleep 30

bash -c "source /home/nvidia/catkin_ws/devel/setup.bash && /opt/ros/melodic/bin/roslaunch planner_agent planner_agent.launch"
# bash -c "source /home/nvidia/catkin_ws/devel/setup.bash && /opt/ros/melodic/bin/rosrun planner_agent planner_agent_node.py"
