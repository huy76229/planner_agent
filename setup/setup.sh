echo "[Setup Dogu Planner Agent]"
echo "[Install Library]"

# Install Pillow
bash -c "python3 -m pip install Pillow"

# Install Cython
bash -c "python3 -m pip install Cython"

# Install numpy
bash -c "python3 -m pip install numpy"

echo "[Finish install library]"

# Copy script file
echo "[Copy start script file]"
bash -c "sudo cp services/planner-start.sh /home/"
echo "[Finish copy script file]"

# Copy service script file
echo "[Copy service file]"
bash -c "sudo cp services/dogu-planner.service /etc/systemd/system/"
echo "[Finish copy service file]"

# Enable service
bash -c "sudo systemctl enable dogu-planner.service"

bash -c "sudo systemctl start dogu-planner.service"
echo "[Finish setup]"