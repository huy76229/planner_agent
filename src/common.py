#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import actionlib
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Imu

import numpy as np

from dbot_msgs.msg import RobotStatus, RobotState, RobotSwitch
from dbot_msgs.msg import DbotControlAction, DbotControlGoal 

from config import *
from functions import *

import time


class RobotCommunication:
    def __init__(self):
        rospy.init_node('planner_agent_node', anonymous=True)

        self.pub_robot_state = rospy.Publisher('/robot_state', RobotState, queue_size=20)
        self.pub_robot_switch = rospy.Publisher('/robot_switch', RobotSwitch, queue_size=20)

        self.client = actionlib.SimpleActionClient('dbot_movebase_action', DbotControlAction)

        # robot position and heading angle
        self.robot_pose = ["0.0", "0.0", "0"]
        # unit: m/s
        self.robot_velocity = "0"

        self.robot_state = IDLE
        self.pre_robot_state_before_hold = IDLE
        self.battery1_value = "0"
        self.battery2_value = "0"
        self.charge_docking_connection = "00"
        self.charger_line_connection = "00"

        self.water_level = "0"
        self.uva_state = "00"
        self.uvc_state = "00"

        self.front_light_state = 0
        self.water_pump_state = 0
        self.uvc_lamp_state = 0
        self.uva_lamp_state = 0
        self.linear_motor_state = 0
        self.rear_light_switch = 0

        self.subscribe_robot()

    def control_uv_lamp(self, uva_ctrl, uvc_ctrl):
        """
        control UVA, UVC lamp
        :param uva_ctrl: left-right
        :param uvc_ctrl: left-right
        """
        robot_switch_ = RobotSwitch()
        robot_switch_.front_light_switch = self.front_light_state
        robot_switch_.water_pump_switch = self.water_pump_state
        robot_switch_.uvc_lamp_switch = self.ctrl_uv(uvc_ctrl)
        robot_switch_.uva_lamp_switch = self.ctrl_uv(uva_ctrl)
        robot_switch_.linear_motor_switch = self.linear_motor_state

        if self.ctrl_uv(uvc_ctrl) > 0:
            if self.ctrl_uv(uva_ctrl) > 0:
                rear_value = 1

            else:
                rear_value = 1

        else:
            if self.ctrl_uv(uva_ctrl) > 0:
                # always on
                rear_value = 2

            else:
                # off
                rear_value = 0

        robot_switch_.rear_light_switch = rear_value

        self.rear_light_switch = rear_value
        self.uvc_lamp_state = self.ctrl_uv(uvc_ctrl)
        self.uva_lamp_state = self.ctrl_uv(uva_ctrl)

        self.uva_state = uva_ctrl
        self.uvc_state = uvc_ctrl

        self.pub_robot_switch.publish(robot_switch_)

        # if DEBUG_TERMINAL:
        #     print("Front Light: ", self.front_light_state)
        #     print("UVC Lamp: ", self.uvc_lamp_state)
        #     print("UVA Lamp: ", self.uva_lamp_state)
        #     print("Pump Switch: ", self.water_pump_state)
        #     print("LiDAR Motor: ", self.linear_motor_state)
        #     print("Rear Light: ", self.rear_light_switch)

    def ctrl_uv(self, uv_value):
        # all off
        if uv_value == "00":
            return 0
        
        # all on
        elif uv_value == "11":
            return 3
        
        # left off
        elif uv_value == "01":
            return 2
        
        # right off
        elif uv_value == "10":
            return 1

    def control_pump(self, pump_control):
        """
        control pump
        """
        # convert pump blower speed
        if float(pump_control) == 0:
            ctrl_pump = 0
        
        elif 0 < float(pump_control) < 0.3:
            ctrl_pump = 1
        
        elif 0.3 <= float(pump_control) < 0.7:
            ctrl_pump = 2
        
        elif 0.7 <= float(pump_control):
            ctrl_pump = 3
        
        robot_switch_ = RobotSwitch()
        robot_switch_.front_light_switch = self.front_light_state
        robot_switch_.water_pump_switch = ctrl_pump
        robot_switch_.uvc_lamp_switch = self.uvc_lamp_state
        robot_switch_.uva_lamp_switch = self.uva_lamp_state
        robot_switch_.linear_motor_switch = self.linear_motor_state
        robot_switch_.rear_light_switch = self.rear_light_switch

        self.water_pump_state = ctrl_pump

        self.pub_robot_switch.publish(robot_switch_)

        # if DEBUG_TERMINAL:
        #     print("Front Light: ", self.front_light_state)
        #     print("UVC Lamp: ", self.uvc_lamp_state)
        #     print("UVA Lamp: ", self.uva_lamp_state)
        #     print("Pump Switch: ", self.water_pump_state)
        #     print("LiDAR Motor: ", self.linear_motor_state)
        #     print("Rear Light: ", self.rear_light_switch)

    def pause_robot(self, pause_ctrl):
        """
        pause robot
        """
        
        robot_state_ = RobotState()

        if pause_ctrl == PAUSE:
            self.pre_robot_state_before_hold = self.robot_state
            robot_state_.robot_state = HOLD

            self.pub_robot_state.publish(robot_state_)

        elif pause_ctrl == PAUSE_CANCEL and self.robot_state == HOLD:
            robot_state_.robot_state = self.pre_robot_state_before_hold

            self.pub_robot_state.publish(robot_state_)
    
    def cancel_goals(self):
        """
        cancel all goals
        """
        self.client.cancel_all_goals()

        # idle state
        robot_state_ = RobotState()
        robot_state_.robot_state = IDLE
        self.pub_robot_state.publish(robot_state_)

    def go_charging(self, position):
        """
        battery charging
        """
        # TODO: to be changed
        # idle state
        robot_state_ = RobotState()
        robot_state_.robot_state = IDLE
        self.pub_robot_state.publish(robot_state_)

        # change robot state
        # sample position
        self.go_here(position)

        if self.robot_state == A2B:
            return True
        
        return False
        
    def release_charging(self):
        """
        release charging, auto change state to IDLE
        """
        # change robot state
        robot_state_ = RobotState()

        # release state
        robot_state_.robot_state = UNDOCKING
        self.pub_robot_state.publish(robot_state_)

    def go_here(self, position):
        """
        go here
        """
        # set robot current position
        current_pos = self.robot_pose

        nodes_list = []
        
        curr_points_ = Pose()
        curr_points_.position.x = float(current_pos[0])
        curr_points_.position.y = float(current_pos[1])
        curr_points_.position.z = 0.0

        curr_yaw = float(current_pos[2]) * np.pi / 180
        curr_q = euler_to_quaternion(0, 0, curr_yaw)

        curr_points_.orientation.x = curr_q[0]
        curr_points_.orientation.y = curr_q[1]
        curr_points_.orientation.z = curr_q[2]
        curr_points_.orientation.w = curr_q[3]

        nodes_list.append(curr_points_)

        # set goal position
        to_point_ = Pose()
        to_point_.position.x = float(position[0])
        to_point_.position.y = float(position[1])
        to_point_.position.z = 0.0

        to_yaw = float(position[2])
        to_q = euler_to_quaternion(0, 0, to_yaw)

        to_point_.orientation.x = to_q[0]
        to_point_.orientation.y = to_q[1]
        to_point_.orientation.z = to_q[2]
        to_point_.orientation.w = to_q[3]

        nodes_list.append(to_point_)

        # print("Go to Goals: ", nodes_list)

        goal = DbotControlGoal()
        goal.waypoints = nodes_list
        self.client.send_goal(goal)

        # change robot state
        robot_state_ = RobotState()
        robot_state_.robot_state = A2B
        self.pub_robot_state.publish(robot_state_)

    def robot_move(self, nodes, moving_state=A2B):
        """
        robot move with path
        """
        # idle state
        robot_state_ = RobotState()
        robot_state_.robot_state = IDLE
        self.pub_robot_state.publish(robot_state_)

        nodes_list = []
        for i in range(len(nodes)):
            path_point = Pose()
            path_point.position.x = float(nodes[i][0])
            path_point.position.y = float(nodes[i][1])
            path_point.position.z = 0.0

            yaw = float(nodes[i][2])
            q = euler_to_quaternion(0, 0, yaw)

            path_point.orientation.x = q[0]
            path_point.orientation.y = q[1]
            path_point.orientation.z = q[2]
            path_point.orientation.w = q[3]

            nodes_list.append(path_point)

        # print("Moving Goals: ", nodes_list)
        
        goal = DbotControlGoal()
        goal.waypoints = nodes_list
        self.client.send_goal(goal)

        # change robot state
        robot_state_ = RobotState()
        robot_state_.robot_state = moving_state
        self.pub_robot_state.publish(robot_state_)

    def robot_quarantine(self, nodes, moving_state=A2B):
        """
        robot quarantine
        """

        # idle state
        robot_state_ = RobotState()
        robot_state_.robot_state = IDLE
        self.pub_robot_state.publish(robot_state_)

        nodes_list = []
        for i in range(len(nodes)):
            path_point = Pose()
            path_point.position.x = float(nodes[i][0])
            path_point.position.y = float(nodes[i][1])
            path_point.position.z = 0.0

            yaw = float(nodes[i][2])
            q = euler_to_quaternion(0, 0, yaw)

            path_point.orientation.x = q[0]
            path_point.orientation.y = q[1]
            path_point.orientation.z = q[2]
            path_point.orientation.w = q[3]

            nodes_list.append(path_point)

        goal = DbotControlGoal()
        goal.waypoints = nodes_list
        self.client.send_goal(goal)

        # change robot state
        robot_state_ = RobotState()
        robot_state_.robot_state = moving_state
        self.pub_robot_state.publish(robot_state_)
    
    def set_robot_state(self, state):
        # set robot state
        robot_state_ = RobotState()
        robot_state_.robot_state = state
        self.pub_robot_state.publish(robot_state_)

    def robot_state_callback(self, msg):
        self.robot_state = msg.robot_state

    def subscribe_robot_state(self):
        """
        subscribe robot state
        """
        rospy.Subscriber("/robot_state", RobotState, self.robot_state_callback)

    def robot_status_callback(self, msg):
        self.battery1_value = str(msg.battery)
        self.battery2_value = str(msg.battery2)
        self.charge_docking_connection = str(msg.charger_contact)
        self.charger_line_connection = str(msg.charger_line)

        # 1: full, 2: normal, 0: empty
        self.water_level = str(msg.water_level)

    def subscribe_robot_status(self):
        """
        subscribe robot status
        """
        rospy.Subscriber("/robot_status", RobotStatus, self.robot_status_callback)

    def robot_pose_callback(self, msg):
        pos_x = msg.position.x
        pos_y = msg.position.y
        pos_z = msg.position.z
        e = quaternion_to_euler(msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w)
        angle_ = e[0] * 180 / np.pi

        if angle_ < 0:
            angle_ = 360 + angle_

        self.robot_pose = [str(pos_x)[:11], str(pos_y)[:11], str(int(angle_))[:3]]

    def subscribe_robot_pose(self):
        """
        subscribe robot pose
        """
        rospy.Subscriber("/robot_pose", Pose, self.robot_pose_callback)
    
    def robot_velocity_callback(self, msg):
        # unit: m/s
        robot_velocity = float(msg.linear_acceleration.x)

        if robot_velocity < 0:
            robot_velocity = 0.00

        self.robot_velocity = str(robot_velocity)[:3]

    def subscribe_robot_velocity(self):
        """
        subscribe robot velocity
        """
        rospy.Subscriber("/imu/data", Imu, self.robot_velocity_callback)

    def subscribe_robot(self):
        """
        subscribe robot
        """
        self.subscribe_robot_pose()
        self.subscribe_robot_velocity()
        self.subscribe_robot_status()
        self.subscribe_robot_state()

        # rospy.Rate(10)  # 10hz
        # rospy.spin()
 
    def get_robot_pose(self):
        return self.robot_pose
    
    def get_robot_velocity(self):
        return self.robot_velocity

    def get_robot_state(self):
        return self.robot_state

    def get_battery1_level(self):
        return self.battery1_value

    def get_battery2_level(self):
        return self.battery2_value

    def get_charge_docking_connect_state(self):
        return self.charge_docking_connection

    def get_charge_line_connect_state(self):
        return self.charger_line_connection

    def get_water_level(self):
        return self.water_level

    def get_uva_state(self):
        return self.uva_state

    def get_uvc_state(self):
        return self.uvc_state

