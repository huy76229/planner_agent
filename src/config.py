# control code 801
CONTROL_CODE = 801
CALL_ROBOT_CODE = "000"
PAUSE_ROBOT_CODE = "100"
SET_ROBOT_PATH_CODE = "102"
CONTROL_UV_LAMP_CODE = "300"
TO_CHARGING_CODE = "301"
TO_QUARANTINE_CODE = "302"
JET_STRENGTH_CODE = "303"
WORK_STOP_CODE = "304"

# info code 802
INFO_CODE = 802
MAP_INFO_CODE = "000"
NODE_INFO_CODE = "100"
PATH_INFO_CODE = "101"

# socket receive buffer size
RECV_MAX_BUF_SIZE = 4294967296

# control cmd
# pause robot
PAUSE = "pause"
PAUSE_CANCEL = "pauseCancel"

# control UV lamp
ON = "1"
OFF = "0"

# debug terminal
DEBUG_TERMINAL = True

# save logs
SAVE_LOG = True

# robot state
IDLE = "IDLE"
A2B = "AtoB"
A2H = "AtoH"
PATROL = "PATROL"
HOLD = "HOLD"
DOCKED = "Docked"
DOCKING = "Docking"
UNDOCKING = "Undocking"

# docking mode
A2C_MODE = "AtoH"

# subcode
ROBOT_NONE_SUBCODE = "0"
ROBOT_INFO_SUBCODE = "3"
ROBOT_CHARGE_SUBCODE = "4"
ROBOT_EVENT_SUBCODE = "5"

# event state
START_WORK_EVENT = "start_work_event"
END_WORK_EVENT = "end_work_event"
CANCEL_WORK_EVENT = "cancel_work_event"
UNDOCKING_EVENT = "undocking_event"
CHARGE_ARLAM_EVENT = "charge_event"
DETECT_OBJECT_EVENT = "detect_obj_event"

SAVE_MAP_LOG = "save_map"
SAVE_NODE_LOG = "save_node"
SAVE_PATH_LOG = "save_path"
PAUSE_LOG = "pause"
PAUSE_CANCEL_LOG = "pause_cancel"
CONTROL_UV_LOG = "control_uv"
RECV_MOVING_TASK_LOG = "moving_cmd"
RUN_MOVING_TASK_LOG = "run_moving_cmd"
RECV_QUARANTINE_TASK_LOG = "quarantine_cmd"
RUN_QUARANTINE_TASK_LOG = "run_quarantine_cmd"
RECV_CHARGING_TASK_LOG = "received_charging_task"
WAIT_CHARGING_TASK_LOG = "stack_charging_task"
REJECT_CHARGING_TASK_LOG = "reject_charging_task"
RELEASE_CHARGING_TASK_LOG = "release_charging_task"
REJECT_RELEASE_CHARGING_TASK_LOG = "reject_release_charging_task"
SET_PUMP_TASK_LOG = "set_pump_strength"
AUTO_OFF_UV_PUMP_LOG = "auto_off_uv_pump"


START_WORK_EVENT_VALUE = "1"
END_WORK_EVENT_VALUE = "0"
CANCEL_WORK_EVENT_VALUE = "2"
UNDOCKING_EVENT_VALUE = "1"
CHARGE_ARLAM_EVENT_VALUE = "1"
DETECT_OBJECT_EVENT_VALUE = "1"

# task working state
WAITING_TASK = "00"
WORKING_TASK = "01"
DONE_TASK = "02"
TASK_ERROR = "99"
CANCEL_TASK = "03"

START_TASK = "start_task"

# robot running state
RUN_WAITING = "00"
RUNNING_STATE = "01"
HOLD_STATE = "02"
EMERGENCY_STOP = "03"

TASK_ASSIGNED = "moving_assigned"
NO_TASK = "no_task"

CHARGING_TASK_ASSIGNED = "charging_assigned"


# task type
QUARANTINE_TASK = "quarantine_task"
CHARGING_TASK = "charging_task"
CHARGING_RELEASE_TASK = "charging_release_task"

TASK_ACCEPT = "accept"
TASK_REJECT = "reject"
TASK_STACKING = "stack"

CHARGING_CMD_WAITING = "cmd_charging_waiting"
CHARGING_AUTO_WAITING = "auto_charging_waiting"
CHARGING_IN_PROGRESS = "charging_in_progress"
CHARGER_RELEASE = "charger_release"
CHARGING_DONE = "charging_done"
IN_CHARGING_MODE = "in_charging_mode"

MOVE_TASK_DOING = "move_task_doing"

# server response
RECV_SUCCESS = "received_success"
NO_ACCESS_KEY = "no_access_key"
MSG_ERROR = "message_error"

RECV_OK = "ok"

END_NODE_POS = "end_node_pos"

DISCONNECT_SESSION = "disconnect_session"
FAIL_CONNECT_SESSION = "fail_connect_session"

# task mode
MOVING_MODE = "102"
QUARANTINE_MODE = "302"

# information type
BASIC_DATA = "basic_data"
CHARGING_DATA = "charging_data"

# battery number
BATTERY_NUM = 2

# unit: (%)
BATTERY_THRESHOLD = -1

CHARGING_BATTERY_THRESHOLD = 95

# check docking time(min)
DOCKING_CHECK_TIME = 0.3
DOCKING_TIME_OUT = 5*60/DOCKING_CHECK_TIME


# update robot information time(min)
ROBOT_INFO_UPDATED_TIME = 1

# update charging information time(min)
CHARGING_INFO_UPDATED_TIME = 180

# charging position
# CHARGING_POSITION_STATION = [2.15, -16.25, 0]
# CHARGING_POSITION_STATION = [8.8238, 9.5675, 0]

HOME_POSITION_STATION = [0, 0, 0]

# check range(unit: meter)
START_RADIUS_RANGE = 0.5
FINISHED_RADIUS_RANGE = 0.5
PASSED_RADIUS_RANGE = 1.0
DOCKING_RADIUS_RANGE = 1.0
