#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import json
import zlib
import yaml
import base64
import os
from PIL import Image
import threading
import math
import time
import logging

from datetime import datetime
from config import *

from common import *
from functions import *

# root directory
root_dir = '/home/nvidia/catkin_ws/src/planner_agent'


# open configure file
with open(root_dir + '/conf/config.yaml') as f:
    config_params = yaml.load(f)


# host server ip and port
HOST = config_params["server_host"]
PORT = config_params["port"]

# robot key
ROBOT_KEY = config_params["robot_key"]

# docking station position
CHARGING_POSITION_STATION = config_params["charging_station_pos"]


# header message length
HEADER_LENGTH = (50*2 + 300*2 + 15*2 + 2*2 + 2*2 + 2*2 + 4*2 + 4*2)

ROBOT_KEY_LENGTH = 50*2
ACCESS_KEY = 300*2

ROBOT_KEY_LENGTH1 = 50
ACCESS_KEY1 = 300
TIME_LENGTH = 15
CMD_LENGTH = 2
INFO_LENGTH = 2
ERROR_LENGTH = 2
CHECKSUM_LENGTH = 4
BODY_LENGTH = 4

BYTES_HEADER_LENGTH = ROBOT_KEY_LENGTH1 + ACCESS_KEY1 + TIME_LENGTH + CMD_LENGTH + INFO_LENGTH + ERROR_LENGTH \
                      + CHECKSUM_LENGTH + BODY_LENGTH


class HeaderFrame:
    def __init__(self):
        self.robot_key = ""
        self.access_key = ""
        self.create_time = ""
        self.cmd_code = 0
        self.info_code = 0
        self.error_code = 0
        self.check_sum = 0
        self.body_length = 0

    def set_robot_key(self, robot_key):
        self.robot_key = robot_key

    def set_access_key(self, access_key):
        self.access_key = access_key

    def set_create_time(self, create_time):
        self.create_time = create_time

    def set_check_sum(self, check_sum):
        self.check_sum = check_sum

    def set_cmd_code(self, cmd_code):
        self.cmd_code = cmd_code

    def set_info_code(self, info_code):
        self.info_code = info_code

    def set_error_code(self, error_code):
        self.error_code = error_code

    def set_body_length(self, body_length):
        self.body_length = body_length

    def get_robot_key(self):
        robot_key_ = self.fill_null_byte(self.robot_key, ROBOT_KEY_LENGTH)
        return robot_key_

    def get_access_key(self):
        access_key_ = self.fill_null_byte(self.access_key, ACCESS_KEY)
        return access_key_

    def get_create_time(self):
        # get current time
        time_ = self.fill_null_byte(self.create_time, 30)
        return time_

    def get_cmd_code(self):
        cmd_code_ = format(self.cmd_code, '04X')
        return cmd_code_

    def get_info_code(self):
        info_code_ = format(self.info_code, '04X')
        return info_code_

    def get_error_code(self):
        error_code_ = format(self.error_code, '04X')
        return error_code_

    def get_checksum(self):
        if type(self.check_sum) is int:
            return format(self.check_sum, '08X')

        elif type(self.check_sum) is str:
            return self.check_sum

    def get_body_length(self):
        body_length_code_ = format(self.body_length, '08X')
        return body_length_code_

    def get_bytes(self, s_data):
        return ''.join(format(ord(x), 'X') for x in s_data)

    def fill_null_byte(self, s_data, s_len):
        b_str = self.get_bytes(s_data)
        if len(b_str) > s_len:
            return ''.join(b_str.split()[i] for i in range(s_len))

        fill_data_len = s_len - len(b_str)
        dest = ''.join('0' for i in range(fill_data_len))

        return b_str + dest

    def get_robot_key_value(self):
        return self.robot_key

    def get_access_key_value(self):
        return self.access_key

    def get_create_time_value(self):
        return self.create_time

    def get_cmd_code_value(self):
        return self.cmd_code

    def get_info_code_value(self):
        return self.info_code

    def get_error_code_value(self):
        return self.error_code

    def get_body_length_value(self):
        return self.body_length


class BodyFrame:
    def __init__(self):
        self.body_data = ""

    def set_body_data(self, body_data):
        self.body_data = body_data

    def get_body(self):
        body_data_code_ = self.get_bytes(self.body_data)
        return body_data_code_

    def get_body_value(self):
        return self.body_data

    def get_body_length(self):
        return len(self.body_data)

    def get_bytes(self, s_data):
        return ''.join(format(ord(x), 'X') for x in s_data)


class KSMEncoder:
    def __init__(self):
        pass

    def make_ksm_data(self, header_frame, body_frame, flag_=False):
        buf = ""
        buf += (header_frame.get_robot_key())
        buf += (header_frame.get_access_key())
        buf += (header_frame.get_create_time())
        buf += (header_frame.get_cmd_code())
        buf += (header_frame.get_info_code())
        buf += (header_frame.get_error_code())
        buf += (header_frame.get_checksum())
        buf += (header_frame.get_body_length())
        buf += (body_frame.get_body())

        if flag_:
            print("<------------Send Message to Server--------------------------->")
            print("Robot Key: ", header_frame.get_robot_key())
            print("Access Key: ", header_frame.get_access_key())
            print("Create Time: ", header_frame.get_create_time())
            print("CMD Code: ", header_frame.get_cmd_code())
            print("Info Code: ", header_frame.get_info_code())
            print("Error Code: ", header_frame.get_error_code())
            print("Checksum: ", header_frame.get_checksum())
            print("Body Length: ", header_frame.get_body_length())
            print("Body value: ", body_frame.get_body())
            print(">---------------------------------------------------------------<")

        return buf


class KSMDecoder:
    def __init__(self):
        self.body_length = 0

    def make_ksm_header_frame(self, byte_arr, header_frame):
        end_idx = 0

        b_length_arr = [ROBOT_KEY_LENGTH1, ACCESS_KEY1, TIME_LENGTH, CMD_LENGTH, INFO_LENGTH, ERROR_LENGTH,
                        CHECKSUM_LENGTH, BODY_LENGTH]

        b_arr = []
        for i in range(len(b_length_arr)):
            start_idx = end_idx
            end_idx = end_idx + b_length_arr[i]
            b_arr.append(byte_arr[start_idx:end_idx])

        header_frame.set_robot_key(self.conv_string(b_arr[0]))
        header_frame.set_access_key(self.conv_string(b_arr[1]))
        header_frame.set_create_time(self.conv_string(b_arr[2]))

        header_frame.set_cmd_code(self.tbyte_arr_to_num(b_arr[3]))
        header_frame.set_info_code(self.tbyte_arr_to_num(b_arr[4]))
        header_frame.set_error_code(self.tbyte_arr_to_num(b_arr[5]))

        header_frame.set_check_sum(self.fbyte_arr_to_num(b_arr[6]))
        header_frame.set_body_length(self.fbyte_arr_to_num(b_arr[7]))

        self.body_length = self.fbyte_arr_to_num(b_arr[7])

        return header_frame

    def make_ksm_body_frame(self, byte_arr, body_frame):
        body_arr_idx = self.body_length

        body_end_ = BYTES_HEADER_LENGTH + body_arr_idx
        body_arr = byte_arr[BYTES_HEADER_LENGTH:body_end_]

        body_arr_data = self.conv_string(body_arr)

        body_frame.set_body_data(body_arr_data)

        return body_frame

    def convert_body_data(self, json_body_data):
        return json.dumps(json_body_data, separators=(',', ':'))

    def conv_string(self, value):
        result_str = ""
        for i in range(len(value)):
            if value[i] != "":
                result_str += chr(value[i])

        return result_str

    def tbyte_arr_to_num(self, value):
        val_1 = hex(value[0])
        val_2 = hex(value[1])
        result = int(val_1, 16) * 256 + int(val_2, 16)
        return result

    def fbyte_arr_to_num(self, value):
        val_1 = hex(value[0])
        val_2 = hex(value[1])
        val_3 = hex(value[2])
        val_4 = hex(value[3])
        result = int(val_1, 16) * 16777216 + int(val_2, 16) * 65536 + int(val_3, 16) * 256 + int(val_4, 16)
        return result


def get_create_time():
    # get current time
    current_time = datetime.now().strftime('%y%m%d%H%M%S%f')[:-3]
    return current_time


def calc_checksum(value):
    checksum_ = zlib.crc32(bytes.fromhex(value))
    return "%08X" % (checksum_ & 0xFFFFFFFF)


def convert_body_data(json_body_data):
    return json.dumps(json_body_data, separators=(',', ':'))


def request_msg(header_frame, body_frame, ksm_encoder, robot_key):
    """
    request access key message
    :param header_frame:
    :param body_frame:
    :param ksm_encoder:
    :param robot_key:
    """
    body_frame.set_body_data("")

    header_frame.set_robot_key(robot_key)
    header_frame.set_access_key("")
    header_frame.set_check_sum(0)
    header_frame.set_cmd_code(100)
    header_frame.set_create_time(get_create_time())
    header_frame.set_error_code(0)
    header_frame.set_info_code(0)
    header_frame.set_body_length(0)

    byte_arr = ksm_encoder.make_ksm_data(header_frame, body_frame, flag_=False)
    # calculate checksum based on CRC32
    header_frame.set_check_sum(calc_checksum(byte_arr))
    byte_arr = ksm_encoder.make_ksm_data(header_frame, body_frame, flag_=True)

    return byte_arr


def check_error_type(error_code):
    if error_code == 0:
        print("Server Response: success")

    if error_code == 100:
        print("Server Response: unauthorized information")

    if error_code == 200:
        print("Server Response: Unauthorized connection")

    if error_code == 301:
        print("Server Response: abnormal message-header abnormal")

    if error_code == 302:
        print("Server Response: abnormal message-length abnormal")


def get_access_key(header_frame):
    """
    get access key
    """

    date_time_obj = datetime.strptime(header_frame.get_create_time_value(), '%y%m%d%H%M%S%f')

    print("\n<--------[Get Access Key] Received Message from Server------------>")
    print("Robot Key: ", header_frame.get_robot_key_value())
    print("Access Key: ", header_frame.get_access_key_value())
    print("Create Time: ", date_time_obj)
    print("CMD Code: ", header_frame.get_cmd_code_value())
    print("Info Code: ", header_frame.get_info_code_value())
    print("Error Code: ", header_frame.get_error_code_value())
    print("Checksum: ", header_frame.get_checksum())
    print("Body Length: ", header_frame.get_body_length_value())
    print(">-----------------------------------------------------------------<")

    # check received error code
    check_error_type(header_frame.get_error_code_value())

    buf = ""
    buf += (header_frame.get_robot_key())
    buf += (header_frame.get_access_key())
    buf += (header_frame.get_create_time())
    buf += (header_frame.get_cmd_code())
    buf += (header_frame.get_info_code())
    buf += (header_frame.get_error_code())
    buf += (header_frame.get_checksum())
    buf += (header_frame.get_body_length())

    received_error_code = header_frame.get_error_code_value()
    if received_error_code == 0:
        return True, header_frame.get_access_key_value()

    elif (received_error_code == 100) or (received_error_code == 200):
        return False, NO_ACCESS_KEY

    else:
        return False, MSG_ERROR


def get_received_msg(header_frame):
    """
    receive message from server
    """

    date_time_obj = datetime.strptime(header_frame.get_create_time_value(), '%y%m%d%H%M%S%f')

    print("\n<------------Received Message from Server--------------------------->")
    print("Robot Key: ", header_frame.get_robot_key_value())
    print("Access Key: ", header_frame.get_access_key_value())
    print("Create Time: ", date_time_obj)
    print("CMD Code: ", header_frame.get_cmd_code_value())
    print("Info Code: ", header_frame.get_info_code_value())
    print("Error Code: ", header_frame.get_error_code_value())
    print("Checksum: ", header_frame.get_checksum())
    print("Body Length: ", header_frame.get_body_length_value())
    print(">-------------------------------------------------------------------<")

    # check received error code
    check_error_type(header_frame.get_error_code_value())

    buf = ""
    buf += (header_frame.get_robot_key())
    buf += (header_frame.get_access_key())
    buf += (header_frame.get_create_time())
    buf += (header_frame.get_cmd_code())
    buf += (header_frame.get_info_code())
    buf += (header_frame.get_error_code())
    buf += (header_frame.get_checksum())
    buf += (header_frame.get_body_length())

    received_error_code = header_frame.get_error_code_value()
    if received_error_code == 0:
        return RECV_SUCCESS

    elif (received_error_code == 100) or (received_error_code == 200):
        return NO_ACCESS_KEY

    else:
        return MSG_ERROR


def compare_checksum(header_frame, body_frame=None):
    """
    check received checksum
    """
    buf = ""
    buf += (header_frame.get_robot_key())
    buf += (header_frame.get_access_key())
    buf += (header_frame.get_create_time())
    buf += (header_frame.get_cmd_code())
    buf += (header_frame.get_info_code())
    buf += (header_frame.get_error_code())
    buf += "00000000"
    buf += (header_frame.get_body_length())

    if body_frame is not None:
        # print("Received access key: ", header_frame.get_access_key())
        buf += body_frame.get_body()

    checksum_ = zlib.crc32(bytes.fromhex(buf))
    checksum_val = "%08X" % (checksum_ & 0xFFFFFFFF)

    # print("Calculate checksum: ", checksum_val)
    # print("Received checksum: ", header_frame.get_checksum())

    if checksum_val == header_frame.get_checksum():
        print("Checksum is valid.")
        return True

    else:
        print("Check sum is not valid.")
        return False


def record_work_log(is_record=True, content_type="", content=""):
    """
    write event log
    :return: save in logs/work_log.txt
    """

    # get current time
    dt = datetime.now().strftime('%Y/%m/%d %H:%M:%S.%f')

    if not os.path.exists(root_dir + "/logs/work_log.txt"):
        if not os.path.isdir(root_dir + "/logs"):
            os.mkdir(root_dir + "/logs")

        # create log file
        with open(root_dir + "/logs/work_log.txt", 'w') as log_file:
            log_file.write("")

    if is_record:
        # clear log file
        read_log_file = open(root_dir + "/logs/work_log.txt", 'r')
        logs_ = read_log_file.readlines()
        read_log_file.close()

        # record event
        with open(root_dir + "/logs/work_log.txt", 'w') as log_file:
            logs_.append("%s\t%s\t%s\n" % (str(dt), str(content_type), str(content)))
            log_file.writelines(logs_)

        log_file.close()

    else:
        # clear log file
        with open(root_dir + "/logs/work_log.txt", 'w') as log_file:
            log_file.write("")

        log_file.close()


def log_insert(text, level):
    """write work logs
    :param format:
    :param text:
    :param level: INFO/ERROR/WARNING
    :return: save in logs/log_20201027.txt
    """
    if SAVE_LOG:
        # log file name
        file = datetime.now().strftime(root_dir + "/logs/log_%Y%m%d.log")

        # log format
        format = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

        info_log = logging.FileHandler(file)
        info_log.setFormatter(format)
        logger = logging.getLogger(file)
        logger.setLevel(level)

        if not logger.handlers:
            logger.addHandler(info_log)
            if level == logging.INFO:
                logger.info(text)

            if level == logging.ERROR:
                logger.error(text)

            if level == logging.WARNING:
                logger.warning(text)

        info_log.close()
        logger.removeHandler(info_log)

        return


class TaskManager:
    def __init__(self):

        # communicate with robot
        self.robot_ros = RobotCommunication()

        # initialize log file
        # record_work_log(is_record=False)

        self.inj_amount = "0"
        self.uva_state = "00"
        self.uvc_state = "00"

        # robot moving mode
        self.moving_task_mode = None

        # update timer update flag
        self.timer_cancel = False

        # docking check flag
        self.docking_check_flag = False

        self.task_assigned = False
        self.to_node_x = 0
        self.to_node_y = 0

        self.from_node_x = 0
        self.from_node_y = 0

        self.nodes_number = 0
        self.finish_nodes = 0
        self.working_nodes_list = []

        self.check_start_task_flag = False
        self.counting_node_flag = False

        self.charging_task_assigned = False
        self.docking_position = [0, 0, 0]

        self.work_state = WAITING_TASK
        self.current_robot_state = RUN_WAITING

        self.update_robot_info_flag = False
        self.task_process_flag = False

        self.waiting_progress = False 
        self.charging_waiting_state = CHARGER_RELEASE

        self.select_upload_info = BASIC_DATA

        self.check_docked_contact_flag_ = False
        self.docking_time_out = 0
        
        self.first_send_charging_data = False
        self.is_sent_charging_data = False
        self.sent_charging_data = ""

        # clear task plan
        self.clear_task_plan()

    def check_robot_working_state(self):
        """
        check task working state every 0.3s
        """

        # update every 0.3s
        check_task_state_timer = threading.Timer(0.3, self.check_robot_working_state)
        check_task_state_timer.start()

        if self.task_assigned:
            print("[Moving] working state...")

            # check start task
            if self.check_start_task() == START_TASK:
                self.counting_node_flag = True

            # counting finished nodes
            self.counting_finished_nodes()

            if self.check_finish_task() == DONE_TASK:
                self.work_state = DONE_TASK

                # wait 2s
                time.sleep(2)

                # send task finish event
                self.task_finished_event()
            
            else:
                if self.work_state != DONE_TASK:
                    self.work_state = WORKING_TASK

                # # robot state
                # robot_state_ = self.robot_ros.get_robot_state()
                # if robot_state_ == IDLE:
                #     self.work_state = WAITING_TASK
                #
                # elif (robot_state_ == PATROL) or (robot_state_ == A2B) or (robot_state_ == A2H):
                #     if self.work_state != DONE_TASK:
                #         self.work_state = WORKING_TASK
                #
                # elif robot_state_ == HOLD:
                #     self.work_state = WORKING_TASK

            # check start task
            # if self.check_start_task() == START_TASK:
            #     if self.moving_task_mode == QUARANTINE_MODE:
            #         # auto on pump
            #         pump_val = "0.5"
            #
            #         # set jet strength
            #         self.robot_ros.control_pump(pump_val)
            #
            #         # update injection amount
            #         self.update_injection_amount(pump_val)
            #
            #         # send task starting event
            #         print("\n>>> [Moving Quarantine] Send Task Starting Event to Server-----------")
            #         task_start_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE,
            #                                                        START_WORK_EVENT)
            #         self.send_robot_info(task_start_event_)
            #
            #     elif self.moving_task_mode == MOVING_MODE:
            #         # send task starting event
            #         print("\n>>> [Moving] Send Task Starting Event to Server------------------")
            #         task_start_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE,
            #                                                        START_WORK_EVENT)
            #         self.send_robot_info(task_start_event_)

        if self.timer_cancel:
            if check_task_state_timer is not None:
                check_task_state_timer.cancel()

    def check_finish_task(self):
        """
        check finish task
        """
        # check range(meter)
        range_radius = FINISHED_RADIUS_RANGE

        # get robot position
        robot_pos_ = self.robot_ros.get_robot_pose()
        curr_x = float(robot_pos_[0])
        curr_y = float(robot_pos_[1])

        if self.finish_nodes > (self.nodes_number - 1):
            if ((self.to_node_x - curr_x) ** 2 + (self.to_node_y - curr_y) ** 2) <= (range_radius ** 2):
                self.to_node_x = 0
                self.to_node_y = 0
                self.nodes_number = 0
                self.finish_nodes = 0
                self.working_nodes_list.clear()

                self.counting_node_flag = False

                # clear moving task plan
                self.clear_moving_task()

                return DONE_TASK
        
        return None

    def check_start_task(self):
        """
        check start task
        """
        # check range(meter)
        range_radius = START_RADIUS_RANGE

        # get robot position
        robot_pos_ = self.robot_ros.get_robot_pose()
        curr_x = float(robot_pos_[0])
        curr_y = float(robot_pos_[1])

        if self.check_start_task_flag:
            if ((self.from_node_x - curr_x) ** 2 + (self.from_node_y - curr_y) ** 2) <= (range_radius ** 2):
                self.from_node_x = 0
                self.from_node_y = 0

                self.check_start_task_flag = False

                return START_TASK

        return None

    def counting_finished_nodes(self):
        """
        counting finish nodes
        """
        # check range(meter)
        range_radius = PASSED_RADIUS_RANGE

        # get robot position
        robot_pos_ = self.robot_ros.get_robot_pose()
        curr_x = float(robot_pos_[0])
        curr_y = float(robot_pos_[1])

        if self.counting_node_flag:
            for node in self.working_nodes_list:
                if ((node[0] - curr_x) ** 2 + (node[1] - curr_y) ** 2) <= (range_radius ** 2):
                    self.finish_nodes = self.finish_nodes + 1
                    self.working_nodes_list.remove(node)

                    break

    def check_robot_pos_with_docking(self):
        """
        check robot position with docking station position every 0.3s
        """

        # update every 0.3s
        check_robot_pos_timer = threading.Timer(DOCKING_CHECK_TIME, self.check_robot_pos_with_docking)
        check_robot_pos_timer.start()

        # check robot is in docking station position or not
        if self.docking_check_flag:
            if self.check_docking_task() == DONE_TASK:
                                
                # print("\n>>> [Charge] Start to Charge------------------")
                #
                # self.first_send_charging_data = True
                #
                # # send robot charging information when robot came to docking station
                # print("\n>>> [First] Send Robot Charging Information to Server------------------")
                # robot_charging_info_ = self.get_robot_information(ROBOT_CHARGE_SUBCODE)
                # self.send_robot_info(robot_charging_info_)
                #
                # # update charging information every 180s
                # self.select_upload_info = CHARGING_DATA
                
                # cancel check docking
                self.docking_check_flag = False

                self.docking_time_out = 0

                # check docking connection state
                self.check_docked_contact_flag_ = True

        # check docking station contact state
        if self.check_docked_contact_flag_:
            # check charger contact state
            robot_state_ = self.robot_ros.get_robot_state()

            if robot_state_ == DOCKED:
                print("\n>>> [Start Charging] Docking is successful. Start to Charge------------------")

                # charging event
                print("\n>>> [Charging Start Event] Send Charge Event to Server................")
                charge_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE, CHARGE_ARLAM_EVENT)
                self.send_robot_info(charge_event_)

                # LOG
                # record_work_log(is_record=True, content_type=CHARGE_ARLAM_EVENT, content=charge_event_)
                log_insert('{}\t{}'.format(CHARGE_ARLAM_EVENT, charge_event_), logging.INFO)

                # wait 0.1s
                time.sleep(0.1)

                self.first_send_charging_data = True
        
                # send robot charging information when robot came to docking station
                print("\n>>> [First Charging Info] Send Robot Charging Information to Server------------------")
                robot_charging_info_ = self.get_robot_information(ROBOT_CHARGE_SUBCODE)
                self.send_robot_info(robot_charging_info_)

                # update charging information every 180s
                self.select_upload_info = CHARGING_DATA

                # change mode to in charging mode
                # self.charging_waiting_state = IN_CHARGING_MODE

                self.check_docked_contact_flag_ = False
                self.docking_time_out = 0

            else:
                # check docking state
                self.docking_time_out += 1

                print("[Check Docking %0.1fs/300s] Robot is docking................" % (self.docking_time_out * DOCKING_CHECK_TIME))

                # cancel docking if time is out(5min)
                if self.docking_time_out > DOCKING_TIME_OUT:
                    print("\n>>> [Fail Charging] Docking is failed------------------")

                    # change robot state to
                    self.robot_ros.set_robot_state(IDLE)

                    # cancel task
                    self.task_assigned = False
                    self.charging_task_assigned = False

                    # cancel start task checking flag
                    self.check_start_task_flag = False

                    # cancel counting node flag
                    self.counting_node_flag = False

                    # clear counting node list
                    self.finish_nodes = 0
                    self.nodes_number = 0

                    self.work_state = CANCEL_TASK

                    # clear task plan
                    self.clear_task_plan()

                    # send task cancel event
                    print("\n>>> [Cancel Task] Send Task Cancel Event to Server------------------")
                    task_cancel_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE, CANCEL_WORK_EVENT)
                    self.send_robot_info(task_cancel_event_)

                    # LOG
                    # record_work_log(is_record=True, content_type=CANCEL_WORK_EVENT, content=task_cancel_event_)
                    log_insert('{}\t{}\t{}'.format(CANCEL_WORK_EVENT, task_cancel_event_, "DOCKING TIMEOUT"), logging.ERROR)

                    self.work_state = WAITING_TASK

                    # charging state
                    self.charging_waiting_state = CHARGER_RELEASE

                    # send basic information
                    self.select_upload_info = BASIC_DATA

                    # clear charging data
                    self.is_sent_charging_data = False
                    self.sent_charging_data = ""

                    # cancel check docking contact
                    self.check_docked_contact_flag_ = False
                    self.docking_time_out = 0

        if self.timer_cancel:
            self.docking_check_flag = False
            self.check_docked_contact_flag_ = False
            if check_robot_pos_timer is not None:
                check_robot_pos_timer.cancel()

    def check_docking_task(self):
        """
        check docking task
        """

        # check range(meter)
        range_radius = DOCKING_RADIUS_RANGE

        # get robot position
        robot_pos_ = self.robot_ros.get_robot_pose()
        curr_x = float(robot_pos_[0])
        curr_y = float(robot_pos_[1])

        self.work_state = WORKING_TASK
        self.charging_waiting_state = CHARGING_IN_PROGRESS

        if ((self.docking_position[0] - curr_x) ** 2 + (self.docking_position[1] - curr_y) ** 2) <= (range_radius ** 2):
            self.docking_position = [0, 0, 0]

            return DONE_TASK
        
        return None

    def off_uv_pump(self):
        """
        turn off all uv and pump
        """
        # off uv lamp
        self.robot_ros.control_uv_lamp("00", "00")
        # update uv lamp state
        self.update_uv_lamp_state("00", "00")

        # off pump
        self.robot_ros.control_pump("0")
        # update injection amount
        self.update_injection_amount("0")

        # write log
        log_insert('{}\t{}'.format(AUTO_OFF_UV_PUMP_LOG, "off_all_uv_pump"), logging.INFO)

    def task_finished_event(self):
        """
        send task finish event
        """

        # send task finish event
        print("\n>>> [Task Finish] Send Task Finish Event to Server------------------")
        task_finish_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE, END_WORK_EVENT)
        self.send_robot_info(task_finish_event_)

        # LOG
        # record_work_log(is_record=True, content_type=END_WORK_EVENT, content=task_finish_event_)
        # record_work_log(is_record=True, content_type="work_state", content=self.work_state)
        log_insert('{}\t{}'.format(END_WORK_EVENT, task_finish_event_), logging.INFO)

        self.task_assigned = False
        self.work_state = WAITING_TASK

        self.moving_task_mode = None
        self.check_start_task_flag = False

        # change robot state
        self.robot_ros.set_robot_state(IDLE)

    def task_process_manager(self):
        """
        task process managing
        """

        # update every 0.3s
        task_timer = threading.Timer(0.3, self.task_process_manager)
        task_timer.start()

        if self.task_process_flag:
            # robot check remaining battery -> auto charging
            if self.charging_waiting_state == CHARGER_RELEASE:
                if not self.charging_task_assigned:
                    self.check_remaining_battery()

            if self.task_assigned:
                if (self.charging_waiting_state == CHARGING_CMD_WAITING) or \
                        (self.charging_waiting_state == CHARGING_AUTO_WAITING):
                    self.waiting_progress = True
            
            if self.waiting_progress:
                print("[Waiting] Waiting for Charging.............................")

                # check working state -> charging
                if (self.work_state == WAITING_TASK or self.work_state == DONE_TASK) and \
                        (self.charging_waiting_state == CHARGING_CMD_WAITING
                         or self.charging_waiting_state == CHARGING_AUTO_WAITING) and not self.task_assigned:

                    self.charging_task_assigned = True

                    if self.charging_waiting_state == CHARGING_AUTO_WAITING:
                        # send charging alarm event
                        # TODO: check sent event is success or not
                        self.auto_charging_alarm_event()

                    # go to charge
                    print("[To Charge] Go to Charge.............................")
                    # self.robot_ros.go_charging(self.docking_position)
                    # change robot state to
                    self.robot_ros.set_robot_state(A2C_MODE)

                    # off uv and pump
                    self.off_uv_pump()

                    # check robot position with docking station position
                    self.docking_check_flag = True
                    
                    # check robot position
                    self.check_robot_pos_with_docking()

                    self.waiting_progress = False

            # if self.charging_waiting_state == IN_CHARGING_MODE:
            #     if self.check_battery_level_charging():
            #         # auto docking release
            #         self.charging_release_action()

            # robot state
            robot_state_ = self.robot_ros.get_robot_state()
            if robot_state_ == IDLE or (robot_state_ == UNDOCKING):
                self.current_robot_state = RUN_WAITING
            
            # elif (robot_state_ == PATROL) or (robot_state_ == A2B) or (robot_state_ == A2H) \
            #         or (robot_state_ == DOCKING) or (robot_state_ == DOCKED):
            #     self.current_robot_state = RUNNING_STATE
            
            elif robot_state_ == HOLD:
                self.current_robot_state = HOLD_STATE

            else:
                self.current_robot_state = RUNNING_STATE

            # else:
            #     self.current_robot_state = EMERGENCY_STOP

        if self.timer_cancel:
            if task_timer is not None:
                print("Cancel Task Process Managing..............")
                task_timer.cancel()

    def check_remaining_battery(self):
        """"
        check battery level
        """
        if BATTERY_NUM == 2:
            # battery 1
            battery1_ = self.robot_ros.get_battery1_level()

            # battery 2
            battery2_ = self.robot_ros.get_battery2_level()

            if int(battery1_) < BATTERY_THRESHOLD or int(battery2_) < BATTERY_THRESHOLD:
                self.auto_charging()

        elif BATTERY_NUM == 1:
            # battery 1
            battery1_ = self.robot_ros.get_battery1_level()

            if int(battery1_) < BATTERY_THRESHOLD:
                self.auto_charging()

    def auto_charging(self):
        """
        auto charging
        """

        # task id
        task_id = "autocharge001"

        # charging position
        charging_station_position = CHARGING_POSITION_STATION

        # check task current
        # current task: no -> charging task
        if self.task_planning(CHARGING_TASK) == TASK_ACCEPT:
            self.charging_task_assigned = True

            task_assigned = "1"

            # update charging task plan
            self.update_charging_task_plan(task_assigned, task_id)

            # send charging alarm event
            self.auto_charging_alarm_event()

            print("[Auto Charging] Go to Charge.......................")
            # self.robot_ros.go_charging(charging_station_position)
            # change robot state to
            self.robot_ros.set_robot_state(A2C_MODE)

            # off uv and pump
            self.off_uv_pump()

            # check robot position
            self.docking_position = charging_station_position

            # check robot position with docking station position
            self.docking_check_flag = True
            self.check_robot_pos_with_docking()

        # check task current
        # current task: yes -> stack charging task(run when current task is done)
        elif self.task_planning(CHARGING_TASK) == TASK_STACKING:
            self.charging_task_assigned = False

            task_assigned = "1"

            # update charging task plan
            self.update_charging_task_plan(task_assigned, task_id)

            # wait for current task finish -> charging
            self.charging_waiting_state = CHARGING_AUTO_WAITING

            # check robot position
            self.docking_position = charging_station_position

    def auto_charging_alarm_event(self):
        """
        send auto charging alarm event
        """

        # charging event
        print("\n>>> [Auto Charging Event] Send Charge Event to Server................")
        auto_charge_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE, CHARGE_ARLAM_EVENT)
        self.send_robot_info(auto_charge_event_)

    def get_robot_information(self, sub_code, event_state=None):
        """
        get robot information
        """

        # get task status
        task_status_ = self.read_task_plan()

        info_msg = dict()

        # get robot position
        robot_pos_ = self.robot_ros.get_robot_pose()
        info_msg["M001"] = robot_pos_[0]
        info_msg["M002"] = robot_pos_[1]
        # robot heading angle
        info_msg["M004"] = robot_pos_[2]

        # robot velocity(m/s)
        robot_vel_ = self.robot_ros.get_robot_velocity()
        info_msg["M003"] = robot_vel_

        info_msg["M007"] = config_params["vehicle_type"]
        info_msg["M008"] = config_params["vehicle_class"]
        info_msg["M009"] = config_params["service_type"]

        # sub code
        # robot default information
        if sub_code == ROBOT_INFO_SUBCODE:
            info_msg["subCode"] = ROBOT_INFO_SUBCODE

            # body
            info_msg["body"] = dict()
            info_msg["body"]["MC001"] = "0"
            info_msg["body"]["MC002"] = "0"
            
            # battery 1
            battery1_ = self.robot_ros.get_battery1_level()
            info_msg["body"]["MC006"] = battery1_

            # estimated driving range 
            info_msg["body"]["MC008"] = "0"

            # robot state
            info_msg["body"]["MC014"] = self.current_robot_state
            
            # from node
            info_msg["body"]["MC015"] = task_status_["task_from_node"]
            # to node
            info_msg["body"]["MC016"] = task_status_["task_to_node"]

            # map id
            info_msg["body"]["MC020"] = task_status_["task_map_id"]
            # map version
            info_msg["body"]["MC021"] = task_status_["task_map_ver"]

            # service
            info_msg["body"]["service"] = dict()

            # task assigned or not
            task_state_ = self.task_assigned
            if task_state_:
                task_assigned_ = "1"
            
            else:
                task_assigned_ = "0"

            info_msg["body"]["service"]["SV001"] = task_assigned_
            # task id
            info_msg["body"]["service"]["SV002"] = task_status_["task_id"]

            # task state
            info_msg["body"]["service"]["SV003"] = self.work_state

            # water level
            water_level_ = self.robot_ros.get_water_level()
            info_msg["body"]["service"]["SV013"] = water_level_

            # uva state
            # uva_state_ = self.robot_ros.get_uva_state()
            info_msg["body"]["service"]["SV014"] = self.uva_state
            
            # disinfectant injection amount
            info_msg["body"]["service"]["SV015"] = self.inj_amount

            # battery 2
            battery2_ = self.robot_ros.get_battery2_level()
            info_msg["body"]["service"]["SV017"] = battery2_

            # uvc state
            # uvc_state_ = self.robot_ros.get_uvc_state()
            info_msg["body"]["service"]["SV018"] = self.uvc_state

            return info_msg
        
        elif sub_code == ROBOT_CHARGE_SUBCODE:
            info_msg["subCode"] = ROBOT_CHARGE_SUBCODE

            # body
            info_msg["body"] = dict()
            # rssi
            info_msg["body"]["MD002"] = "0"

            # error code
            # TODO: check battery number
            info_msg["body"]["MD003"] = "BT11"

            # battery 1
            battery1_ = self.robot_ros.get_battery1_level()
            info_msg["body"]["MD004"] = battery1_

            # estimated driving range 
            info_msg["body"]["MD006"] = "0"

            # charge capacity
            info_msg["body"]["MD007"] = "0"

            # charger contact state
            charger_contact_state_ = self.robot_ros.get_charge_docking_connect_state()
            info_msg["body"]["MD008"] = (charger_contact_state_ + charger_contact_state_)

            # charging completion estimated time
            info_msg["body"]["MD009"] = "0"

            # battery 2
            battery2_ = self.robot_ros.get_battery2_level()
            info_msg["body"]["MD015"] = battery2_

            return info_msg
        
        elif sub_code == ROBOT_EVENT_SUBCODE:
            info_msg["subCode"] = ROBOT_EVENT_SUBCODE

            # body
            info_msg["body"] = dict()

            # AIV body
            info_msg["body"]["EVTAIV001"] = dict()

            # working state
            # TODO: check current working state
            if event_state == START_WORK_EVENT:
                info_msg["body"]["EVTAIV001"]["EVT001"] = START_WORK_EVENT_VALUE
            
            elif event_state == END_WORK_EVENT:
                info_msg["body"]["EVTAIV001"]["EVT001"] = END_WORK_EVENT_VALUE
            
            elif event_state == CANCEL_WORK_EVENT:
                info_msg["body"]["EVTAIV001"]["EVT001"] = CANCEL_WORK_EVENT_VALUE
            
            elif event_state == UNDOCKING_EVENT:
                info_msg["body"]["EVTAIV001"]["EVT002"] = UNDOCKING_EVENT_VALUE
            
            elif event_state == CHARGE_ARLAM_EVENT:
                info_msg["body"]["EVTAIV001"]["EVT003"] = CHARGE_ARLAM_EVENT_VALUE
            
            return info_msg
        
        return ""

    def request_access_key(self):
        """
        request access key from server
        :param s_obj:
        :param header_frame:
        :param body_frame:
        :param ksm_encoder:
        :param ksm_decoder:
        :param robot_key:
        :return: access key
        """

        request_bytes_data = bytes.fromhex(request_msg(self.header_frame, self.body_frame, self.ksm_encoder, self.robot_key))

        print("\n>>> Send request access key---------------------------")

        try:
            # send data to server
            self.s_obj.sendall(request_bytes_data)

        except:
            return DISCONNECT_SESSION
        
        # wait 0.1s
        time.sleep(0.1)

        # receive response from the server
        msg = b''
        try:
            msg = self.s_obj.recv(4096)

        except socket.timeout:
            msg = b''

        except:
            pass

        if len(msg) > 0:
            header = self.ksm_decoder.make_ksm_header_frame(msg, self.header_frame)
            body = self.ksm_decoder.make_ksm_body_frame(msg, self.body_frame)

            # check received data checksum
            if compare_checksum(header):
                state_, access_key_ = get_access_key(header)

                if state_ is True:
                    msg = b''
                    return access_key_

                else:
                    msg = b''
                    return None
            
            msg = b''

        return None
        
    def get_access_key(self):
        """
        get access key
        """
        while True:
            # request access key
            print(">>> Request Access Key.....")
            access_key = self.request_access_key()

            if access_key is not None and access_key != DISCONNECT_SESSION:
                return access_key
            
            if access_key == DISCONNECT_SESSION:
                return DISCONNECT_SESSION

            # wait 1s
            time.sleep(1)

    def communication_setting(self, header_frame, body_frame, ksm_encoder, ksm_decoder, robot_key):
        """
        set msg parameter for communication
        """
        self.header_frame = header_frame
        self.body_frame = body_frame
        self.ksm_encoder = ksm_encoder
        self.ksm_decoder = ksm_decoder
        self.robot_key = robot_key

        # connect to session
        self.reconnect_session(is_first_conn=True)

    def reconnect_session(self, is_first_conn=False):
        """
        reconnect to session
        """

        get_key_ = False
        while True:
            # disable update information to server
            self.update_robot_info_flag = False

            # connect to session
            while True:
                is_connect_ = self.connect_session()
                if is_connect_:
                    get_key_ = True
                    break

                # wait 1s
                time.sleep(1)

            if get_key_:
                # don't recv data
                self.flag_ = False

                access_key_ = self.get_access_key()

                # LOG
                # record_work_log(is_record=True, content_type="request_access_key", content=access_key_)
                log_insert('{}\t{}'.format("request_access_key", access_key_), logging.INFO)

                if access_key_ != DISCONNECT_SESSION:
                    get_key_ = False

                    self.access_key = access_key_

                    if not is_first_conn:

                        # resend robot basic information
                        print("\n>>> [Reconnect Session] Resend Robot Information to Server..........")
                        robot_info_ = self.get_robot_information(ROBOT_INFO_SUBCODE)
                        print("Resend Robot Info: ", robot_info_)

                        try:
                            # send basic info data to server
                            self.s_obj.sendall(bytes.fromhex(self.send_data(robot_info_)))
                        
                        except:
                            pass
                        
                        # wait 0.1s
                        time.sleep(0.1)

                        # receive response from the server
                        msg = b''
                        try:
                            msg = self.s_obj.recv(4096)

                        except:
                            msg = b''
                            pass

                        if len(msg) > 0:
                            header_msg = self.ksm_decoder.make_ksm_header_frame(msg, self.header_frame)
                            body_msg = self.ksm_decoder.make_ksm_body_frame(msg, self.body_frame)

                            # check received data checksum
                            if compare_checksum(header_msg, body_msg):
                                if get_received_msg(header_msg) == RECV_SUCCESS:

                                    msg = b''

                                    # resend charging data to server if session is not connected
                                    if self.select_upload_info == CHARGING_DATA and self.is_sent_charging_data:

                                        print(">>> [Resend Data] Resent charging data.........")
                                        print("Resent charging data: ", self.sent_charging_data)
                                        try:
                                            # send charging data to server
                                            self.s_obj.sendall(bytes.fromhex(self.send_data(self.sent_charging_data)))

                                        except:
                                            pass
                                        
                                        # wait 0.1s
                                        time.sleep(0.1)
                                        
                                        # receive response from the server
                                        charging_msg = b''
                                        try:
                                            charging_msg = self.s_obj.recv(4096)

                                        except:
                                            charging_msg = b''
                                            pass
                                        
                                        if len(charging_msg) > 0:
                                            header_msg = self.ksm_decoder.make_ksm_header_frame(charging_msg, self.header_frame)
                                            body_msg = self.ksm_decoder.make_ksm_body_frame(charging_msg, self.body_frame)

                                            # check received data checksum
                                            if compare_checksum(header_msg, body_msg):
                                                if get_received_msg(header_msg) == RECV_SUCCESS:
                                                    # recv data
                                                    self.flag_ = True

                                                    self.is_sent_charging_data = False
                                                    self.sent_charging_data = ""

                                                    # enable update information to server
                                                    self.update_robot_info_flag = True

                                                    charging_msg = b''
                                                    break

                                            charging_msg = b''
                                    
                                    else:
                                        # recv data
                                        self.flag_ = True

                                        # enable update information to server
                                        self.update_robot_info_flag = True

                                        break

                            msg = b''

                    else:
                        # recv data
                        self.flag_ = True

                        # enable update information to server
                        self.update_robot_info_flag = True

                        break

                else:
                    # recv data
                    self.flag_ = True

                    get_key_ = False
        
    # def check_connect_session(self):
    #     """
    #     check connect to session
    #     """
    #     print(">>> Re-connect session.....")
    #     # connect to session
    #     is_connect_ = self.connect_session()
    #     if not is_connect_:
    #         return FAIL_CONNECT_SESSION
    #
    #     # request access key
    #     access_key = request_access_key(self.s_obj, self.header_frame, self.body_frame, self.ksm_encoder,
    #                                     self.ksm_decoder, self.robot_key)
    #
    #     if access_key is not None and access_key != DISCONNECT_SESSION:
    #         return access_key
    #
    #     else:
    #         return FAIL_CONNECT_SESSION
    #
    # def check_in_home_pos(self):
    #     """
    #     check robot is in home position or not
    #     """
    #     # get robot position
    #     robot_pos_ = self.robot_ros.get_robot_pose()
    #     curr_x = float(robot_pos_[0])
    #     curr_y = float(robot_pos_[1])
    #
    #     # home position
    #     home_pos = HOME_POSITION_STATION
    #
    #     # radius: 1m
    #     range_radius = 1
    #
    #     if ((home_pos[0] - curr_x) ** 2 + (home_pos[1] - curr_y) ** 2) <= (range_radius ** 2):
    #         return True
    #
    #     else:
    #         return False
    #
    # def reconnect_session(self):
    #     """
    #     reconnect to session
    #     """
    #     time_n = 0
    #     while True:
    #         # check session connection
    #         is_connect_session = self.check_connect_session()
    #         if is_connect_session == FAIL_CONNECT_SESSION:
    #             # check robot is in home or shelter ???
    #
    #             if self.check_in_home_pos():
    #                 missing_alarm = 0
    #
    #                 # cancel current all goals
    #                 self.robot_ros.cancel_goals()
    #
    #                 # cancel task
    #                 self.task_assigned = False
    #                 self.charging_task_assigned = False
    #
    #                 # cancel start task checking flag
    #                 self.check_start_task_flag = False
    #
    #                 # clear task plan
    #                 self.clear_task_plan()
    #
    #                 self.work_state = WAITING_TASK
    #
    #                 # charging state
    #                 self.charging_waiting_state = CHARGER_RELEASE
    #
    #                 # off all uv and pump
    #                 self.off_uv_pump()
    #
    #                 # send basic information
    #                 self.select_upload_info = BASIC_DATA
    #
    #                 # accumulated waiting time 9min
    #                 if time_n > 8:
    #                     time_n = 0
    #
    #                 time.sleep(2 ** time_n)
    #                 time_n+=1
    #
    #             else:
    #                 if self.check_current_task() == TASK_ASSIGNED:
    #                     # accumulated waiting time 9min
    #                     if time_n > 7:
    #                         time_n = 0
    #
    #                         # cancel current all goals
    #                         self.robot_ros.cancel_goals()
    #
    #                         # cancel task
    #                         self.task_assigned = False
    #                         self.charging_task_assigned = False
    #
    #                         # cancel start task checking flag
    #                         self.check_start_task_flag = False
    #
    #                         # clear task plan
    #                         self.clear_task_plan()
    #
    #                         self.work_state = WAITING_TASK
    #
    #                         # charging state
    #                         self.charging_waiting_state = CHARGER_RELEASE
    #
    #                         # off all uv and pump
    #                         self.off_uv_pump()
    #
    #                         # send basic information
    #                         self.select_upload_info = BASIC_DATA
    #
    #                         # go home
    #                         self.robot_ros.set_robot_state(A2H)
    #
    #                         # time out 22min
    #                         time_out = 22
    #                         check_time_out_ = 0
    #                         while True:
    #                             if self.check_in_home_pos():
    #                                 break
    #
    #                             if check_time_out_ >= time_out:
    #                                 missing_alarm = 1
    #                                 break
    #
    #                             time.sleep(1)
    #                             check_time_out_+=1
    #
    #                     else:
    #                         # pause robot
    #                         self.robot_ros.pause_robot(PAUSE)
    #
    #                         time.sleep(2 ** time_n)
    #                         time_n+=1
    #
    #                 else:
    #                     if missing_alarm:
    #                         print("Can not go to home or shelter!")
    #                         # find new home/shelter -> go
    #
    #                     elif time_n > 7:
    #                         time_n = 0
    #
    #                         # time out 22min
    #                         time_out = 22
    #                         check_time_out_ = 0
    #                         while True:
    #                             if self.check_in_home_pos():
    #                                 break
    #
    #                             if check_time_out_ >= time_out:
    #                                 check_time_out_ = 1
    #                                 break
    #
    #                             time.sleep(1)
    #                             check_time_out_+=1
    #
    #                     else:
    #                         time.sleep(2 ** time_n)
    #                         time_n+=1
    #         else:
    #             time_n = 0
    #             missing_alarm = 0
    #             return is_connect_session

    def update_robot_basic_info(self):
        """
        update robot basic information to server
        """

        # update every 1s
        update_timer = threading.Timer(ROBOT_INFO_UPDATED_TIME, self.update_robot_basic_info)
        update_timer.start()

        if self.select_upload_info == BASIC_DATA:
            if self.update_robot_info_flag:
                # send robot information
                print("\n>>> [%ss] Send Robot Information to Server----------" % str(ROBOT_INFO_UPDATED_TIME))
                robot_info_ = self.get_robot_information(ROBOT_INFO_SUBCODE)
                self.send_robot_info(robot_info_)
            
        if self.timer_cancel:
            if update_timer is not None:
                print("Update Cancel ..............")
                update_timer.cancel()  
        
    def update_robot_charging_info(self):
        """
        update robot charging information
        """

        # update every 180s
        update_bat_timer = threading.Timer(CHARGING_INFO_UPDATED_TIME, self.update_robot_charging_info)
        update_bat_timer.start()

        if self.select_upload_info == CHARGING_DATA:
            if self.update_robot_info_flag:
                # send robot charging information
                print("\n>>> [%ss] Send Robot Charging Information to Server-------" % str(CHARGING_INFO_UPDATED_TIME))
                robot_charging_info_ = self.get_robot_information(ROBOT_CHARGE_SUBCODE)
                self.send_robot_info(robot_charging_info_)
           
        if self.timer_cancel:
            if update_bat_timer is not None:
                print("Update Battery Cancel ..............")
                update_bat_timer.cancel()   

    def set_update_robot_info_flag(self, flag):
        """
        set update timer flag
        """
        self.update_robot_info_flag = flag

        # run task process manager
        self.task_process_flag = flag

    def cancel_timer(self, flag):
        """
        cancel timer
        """
        self.timer_cancel = flag

    def server_message_process(self):
        """
        server message set 801/802
        """

        if self.header_frame.get_info_code_value() == INFO_CODE:
            body_data = json.loads(self.body_frame.get_body_value())

            if list(body_data.keys()).count("subCode") > 0:
                if body_data["subCode"] == MAP_INFO_CODE:        # code: 000
                    self.save_map_image(body_data)

                    # send robot response
                    self.send_response_from_robot(info_code=INFO_CODE)

                elif body_data["subCode"] == NODE_INFO_CODE:     # code: 100
                    if self.save_nodes_data(body_data):
                        # send robot response
                        self.send_response_from_robot(info_code=INFO_CODE)
                    
                    else:
                        # send robot response(no have map folder, must send map before sending nodes)
                        self.send_response_from_robot(error_code=301, info_code=INFO_CODE)
                    
                elif body_data["subCode"] == PATH_INFO_CODE:     # code: 101
                    if self.save_paths_data(body_data):
                        # send robot response
                        self.send_response_from_robot(info_code=INFO_CODE)
                    
                    else:
                        # send robot response(no have map folder, must send map before sending paths)
                        self.send_response_from_robot(error_code=301, info_code=INFO_CODE)
                    
        elif self.header_frame.get_info_code_value() == CONTROL_CODE:
            body_data = json.loads(self.body_frame.get_body_value())

            if list(body_data.keys()).count("subCode") > 0:
                if body_data["subCode"] == CALL_ROBOT_CODE:        # code: 000
                    self.call_here(body_data)

                elif body_data["subCode"] == PAUSE_ROBOT_CODE:       # code: 100
                    self.server_pause_robot(body_data)

                elif body_data["subCode"] == SET_ROBOT_PATH_CODE:    # code: 102
                    self.move_robot(MOVING_MODE, body_data)

                elif body_data["subCode"] == CONTROL_UV_LAMP_CODE:   # code: 300
                    self.server_control_uv_lamp(body_data)

                elif body_data["subCode"] == TO_CHARGING_CODE:    # code: 301
                    self.go_to_charging(body_data)
                    
                elif body_data["subCode"] == TO_QUARANTINE_CODE:   # code: 302
                    self.move_robot(QUARANTINE_MODE, body_data)

                elif body_data["subCode"] == JET_STRENGTH_CODE:  # code: 303
                    self.set_jet_strength(body_data)
                                    
                elif body_data["subCode"] == WORK_STOP_CODE:   # code: 304
                    self.stop_all_work(body_data)

    def call_here(self, body_data):
        """
        call robot to go here
        """
        # TODO: add function
        print(">> Call Here ----------------------------------------")
        pass
  
    def stop_all_work(self, body_data):
        """
        stop all current work
        """
        ctrl_cmd = body_data["body"]["001"]

        if ctrl_cmd == "1":
            # cancel current all goals
            self.robot_ros.cancel_goals()

            # cancel task
            self.task_assigned = False

            self.charging_task_assigned = False

            # cancel waiting flag
            self.waiting_progress = False

            # cancel start task checking flag
            self.check_start_task_flag = False

            # cancel counting node flag
            self.counting_node_flag = False

            # clear counting node list
            self.finish_nodes = 0
            self.nodes_number = 0

            # cancel check docking contact
            self.check_docked_contact_flag_ = False

            self.work_state = CANCEL_TASK

            # clear task plan
            self.clear_task_plan()

            # send task cancel event
            print("\n>>> [Cancel Task] Send Task Cancel Event to Server------------------")
            task_cancel_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE, CANCEL_WORK_EVENT)
            self.send_robot_info(task_cancel_event_)

            # LOG
            # record_work_log(is_record=True, content_type=CANCEL_WORK_EVENT, content=task_cancel_event_)
            log_insert('{}\t{}\t{}'.format(CANCEL_WORK_EVENT, task_cancel_event_, "JOB CANCEL"), logging.INFO)

            self.work_state = WAITING_TASK

            # charging state
            self.charging_waiting_state = CHARGER_RELEASE

            # off all uv and pump
            self.off_uv_pump()

            # send basic information
            self.select_upload_info = BASIC_DATA

            # clear charging data
            self.is_sent_charging_data = False
            self.sent_charging_data = ""

    def server_pause_robot(self, body_data):
        """
        pause robot
        """
        ctrl_cmd = body_data["body"]["001"]
        
        # pause robot
        self.robot_ros.pause_robot(ctrl_cmd)

        # send robot response
        self.send_response_from_robot(info_code=CONTROL_CODE)

        if ctrl_cmd == PAUSE:
            print("\n>>> [Pause] Pause Robot ------------------")
            # write log
            log_insert('{}\t{}'.format(PAUSE_LOG, "pause_robot"), logging.INFO)
        
        elif ctrl_cmd == PAUSE_CANCEL:
            print("\n>>> [Cancel Pause] Cancel Pause Robot ------------------")
            # write log
            log_insert('{}\t{}'.format(PAUSE_CANCEL_LOG, "cancel_pause_robot"), logging.INFO)

    def server_control_uv_lamp(self, body_data):
        """
        control uv lamp
        """
        ctrl_uva = body_data["body"]["001"]
        ctrl_uvc = body_data["body"]["002"]

        ctrl_uva_left = ctrl_uva[0]
        ctrl_uva_right = ctrl_uva[1]

        ctrl_uvc_left = ctrl_uvc[0]
        ctrl_uvc_right = ctrl_uvc[1]

        # control uv lamp
        self.robot_ros.control_uv_lamp(ctrl_uva, ctrl_uvc)

        # send robot response
        self.send_response_from_robot(info_code=CONTROL_CODE)

        # update uv lamp state
        self.update_uv_lamp_state(ctrl_uva, ctrl_uvc)

        print("\n>>> [UV Lamp] Control UV Lamp ------------------")
        print(">> [UVA] Left: %s / Right: %s" % (ctrl_uva_left, ctrl_uva_right))
        print(">> [UVC] Left: %s / Right: %s" % (ctrl_uvc_left, ctrl_uvc_right))

        # write log
        info_ = "uva_left:{}_right:{}_/uvc_left:{}_right:{}".format(ctrl_uva_left, ctrl_uva_right,
                                                                    ctrl_uvc_left, ctrl_uvc_right)
        log_insert('{}\t{}'.format(CONTROL_UV_LOG, info_), logging.INFO)

    def move_robot(self, task_mode, body_data):
        """
        move robot
        """
        start_node = body_data["body"]["001"]
        end_node = body_data["body"]["002"]

        task_id = body_data["SA003"]
        
        move_path = []
        nodes_list = []

        if list(body_data["body"].keys()).count("003") > 0:
            pass_nodes = body_data["body"]["003"]
            nodes_list = pass_nodes.split(",")

        if len(nodes_list) > 0:
            move_path = nodes_list
            move_path.insert(0, start_node)
            move_path.append(end_node)

        else:
            move_path.append(start_node)
            move_path.append(end_node)

        print("\n>>> [Received Moving Nodes] Robot Move------------------")
        print(">>> Moving Nodes List: ", move_path)

        # write log
        if task_mode == MOVING_MODE:
            info_ = "nodes_list: {}".format(move_path)
            log_insert('{}\t{}'.format(RECV_MOVING_TASK_LOG, info_), logging.INFO)

        if task_mode == QUARANTINE_MODE:
            info_ = "nodes_list: {}".format(move_path)
            log_insert('{}\t{}'.format(RECV_QUARANTINE_TASK_LOG, info_), logging.INFO)

        # send robot response
        self.send_response_from_robot(info_code=CONTROL_CODE)

        # check task current
        if self.task_planning(QUARANTINE_TASK) == TASK_ACCEPT:
            self.task_assigned = True

            task_assigned = "1"

            # update task plan
            self.update_task_plan(task_assigned, task_id, start_node, end_node)

            # run task
            self.run_task(task_mode=task_mode, nodes_list=move_path)

    def run_task(self, task_mode, nodes_list):
        """
        run task
        """
        
        task_nodes_list = []

        working_task_ = self.read_task_plan()
        if working_task_ is not None:
            map_folder_ = working_task_["task_map_id"]

            if len(map_folder_) > 0:
                task_map_path_ = root_dir + "/maps/" + map_folder_
                if os.path.exists(task_map_path_):
                    nodes_path_ = task_map_path_ + "/%s_nodes.json" % str(map_folder_)

                    if os.path.exists(nodes_path_):
                         with open(nodes_path_) as json_file:
                            nodes_data = json.load(json_file)
                            nodes_ = nodes_data["nodes"]
                            
                            # make nodes list
                            for task_node in nodes_list:
                                for i, saved_node in enumerate(nodes_):
                                    if saved_node["nodeId"] == task_node:
                                        task_nodes_list.append([float(saved_node["x"]), float(saved_node["y"]), 0.0])
                            
                            # check nodes number
                            if len(task_nodes_list) >= 2:
                                planning_task_nodes = self.calculate_theta_angle(task_nodes_list)

                                if len(planning_task_nodes) > 2:
                                    
                                    # end node
                                    self.to_node_x = planning_task_nodes[len(planning_task_nodes)-1][0]
                                    self.to_node_y = planning_task_nodes[len(planning_task_nodes)-1][1]
                                    self.nodes_number = len(planning_task_nodes)
                                    self.working_nodes_list = planning_task_nodes

                                    # start node
                                    self.from_node_x = planning_task_nodes[0][0]
                                    self.from_node_y = planning_task_nodes[0][1]

                                    if task_mode == MOVING_MODE:
                                        # write log
                                        info_ = "nodes_list_len: {}".format(len(planning_task_nodes))
                                        log_insert('{}\t{}'.format(RUN_MOVING_TASK_LOG, info_), logging.INFO)

                                        # control robot
                                        self.robot_ros.robot_move(planning_task_nodes, moving_state=A2B)

                                        # check task state
                                        self.task_assigned = True

                                        # robot moving mode
                                        self.moving_task_mode = MOVING_MODE

                                        # send basic information
                                        self.select_upload_info = BASIC_DATA

                                        # check start task
                                        self.check_start_task_flag = True

                                        # send task starting event
                                        print("\n>>> [Moving] Send Task Starting Event to Server------------------")
                                        task_start_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE,
                                                                                       START_WORK_EVENT)
                                        self.send_robot_info(task_start_event_)

                                        # LOG
                                        # record_work_log(is_record=True, content_type=START_WORK_EVENT,
                                        #                 content=task_start_event_)
                                        log_insert(
                                            '{}\t{}\t{}'.format(START_WORK_EVENT, task_start_event_, "START MOVE"),
                                            logging.INFO)

                                    if task_mode == QUARANTINE_MODE:
                                        # write log
                                        info_ = "nodes_list_len: {}".format(len(planning_task_nodes))
                                        log_insert('{}\t{}'.format(RUN_QUARANTINE_TASK_LOG, info_), logging.INFO)

                                        # control robot
                                        self.robot_ros.robot_quarantine(planning_task_nodes, moving_state=A2B)

                                        # check task state
                                        self.task_assigned = True

                                        # robot moving mode
                                        self.moving_task_mode = QUARANTINE_MODE

                                        # send basic information
                                        self.select_upload_info = BASIC_DATA

                                        # check start task
                                        self.check_start_task_flag = True

                                        # send task starting event
                                        print("\n>>> [Moving Quarantine] Send Task Starting Event to Server-----------")
                                        task_start_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE,
                                                                                       START_WORK_EVENT)
                                        self.send_robot_info(task_start_event_)

                                        # LOG
                                        # record_work_log(is_record=True, content_type=START_WORK_EVENT,
                                        #                 content=task_start_event_)
                                        log_insert(
                                            '{}\t{}\t{}'.format(START_WORK_EVENT, task_start_event_, "START QUARANTINE"),
                                            logging.INFO)
                            
                            print("\n>>> [Moving Error] Check node id or control mode ------------------")

    def calculate_theta_angle(self, nodes_list):
        """
        calculate theta angle(rad)
        """

        if len(nodes_list) > 2:
            for i in range(1, len(nodes_list)):
                pos1_x = nodes_list[i - 1][0]
                pos1_y = nodes_list[i - 1][1]
                
                pos2_x = nodes_list[i][0]
                pos2_y = nodes_list[i][1]

                if i == 1:
                    start_pos_x = pos1_x
                    start_pos_y = pos1_y

                if i == (len(nodes_list) - 1):
                    end_pos_x = pos2_x
                    end_pos_y = pos2_y

                # get waypoint orientation
                angle_ = round(math.atan2(pos2_y - pos1_y, pos2_x - pos1_x), 3)

                nodes_list[i][2] = angle_

            angle_ = round(math.atan2(end_pos_y - start_pos_y, end_pos_x - start_pos_x), 3)
            nodes_list[0][2] = angle_

            return nodes_list
        
        elif len(nodes_list) == 2:
            pos1_x = nodes_list[0][0]
            pos1_y = nodes_list[0][1]
            
            pos2_x = nodes_list[1][0]
            pos2_y = nodes_list[1][1]

            angle_ = round(math.atan2(pos2_y - pos1_y, pos2_x - pos1_x), 3)

            # create new waypoint
            mid_pos_x = (pos1_x + pos2_x) / 2
            mid_pos_y = (pos1_y + pos2_y) / 2

            new_nodes_list = []
            new_nodes_list.append([pos1_x, pos1_y, angle_])
            new_nodes_list.append([mid_pos_x, mid_pos_y, angle_])
            new_nodes_list.append([pos2_x, pos2_y, angle_])

            return new_nodes_list

    def go_to_charging(self, body_data):
        """
        go to charging
        """

        ctrl_cmd = body_data["body"]["001"]

        # charging task id
        task_id = body_data["SA003"]

        # send robot response
        self.send_response_from_robot()

        # charging position
        charging_station_position = CHARGING_POSITION_STATION

        if ctrl_cmd == ON:
            # check task current
            # current task: no -> charging task
            if self.task_planning(CHARGING_TASK) == TASK_ACCEPT:
                self.charging_task_assigned = True

                task_assigned = "1"

                # update charging task plan
                self.update_charging_task_plan(task_assigned, task_id)

                print("[Charging] Go to Charge..........................................")
                # self.robot_ros.go_charging(charging_station_position)
                # change robot state to
                self.robot_ros.set_robot_state(A2C_MODE)

                # write log
                info_ = "task id: {}".format(task_id)
                log_insert('{}\t{}'.format(RECV_CHARGING_TASK_LOG, info_), logging.INFO)

                # off uv and pump
                self.off_uv_pump()

                # check robot position
                self.docking_position = charging_station_position
                self.docking_check_flag = True
                self.check_robot_pos_with_docking()

            # check task current
            # current task: yes -> stack charging task(run when current task is done)
            elif self.task_planning(CHARGING_TASK) == TASK_STACKING:
                self.charging_task_assigned = False

                task_assigned = "1"

                # update charging task plan
                self.update_charging_task_plan(task_assigned, task_id)

                # wait for current task finish -> charging
                self.charging_waiting_state = CHARGING_CMD_WAITING

                # check robot position
                self.docking_position = charging_station_position

                # write log
                info_ = "task id: {}".format(task_id)
                log_insert('{}\t{}'.format(WAIT_CHARGING_TASK_LOG, info_), logging.INFO)

            # reject task
            elif self.task_planning(CHARGING_TASK) == TASK_REJECT:
                # write log
                info_ = "task id: {}".format(task_id)
                log_insert('{}\t{}'.format(REJECT_CHARGING_TASK_LOG, info_), logging.ERROR)

        elif ctrl_cmd == OFF:
            # check task current
            if self.task_planning(CHARGING_RELEASE_TASK) == TASK_ACCEPT:
                self.charging_task_assigned = False

                # clear charging task
                self.clear_charging_task()

                print("[Charging Release] Charge Release..........................")
                self.robot_ros.release_charging()

                # write log
                info_ = "release_docking"
                log_insert('{}\t{}'.format(RELEASE_CHARGING_TASK_LOG, info_), logging.INFO)

                # TODO: check connect release???
                # send charge release event
                print("\n>>> [Charge Release] Send Robot Charge Release Event to Server.............")
                charge_release_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE, UNDOCKING_EVENT)
                self.send_robot_info(charge_release_event_)

                # LOG
                # record_work_log(is_record=True, content_type=UNDOCKING_EVENT, content=charge_release_event_)
                log_insert('{}\t{}'.format(UNDOCKING_EVENT, charge_release_event_), logging.INFO)

                # change to waiting state
                self.work_state = WAITING_TASK

                # charging state
                self.charging_waiting_state = CHARGER_RELEASE

                # send basic information
                self.select_upload_info = BASIC_DATA

                # clear charging data
                self.is_sent_charging_data = False
                self.sent_charging_data = ""

            # reject task
            elif self.task_planning(CHARGING_RELEASE_TASK) == TASK_REJECT:
                # write log
                log_insert('{}\t{}'.format(REJECT_RELEASE_CHARGING_TASK_LOG, "reject_release"), logging.ERROR)

    def charging_release_action(self):
        """
        charging release action
        """
        if self.task_planning(CHARGING_RELEASE_TASK) == TASK_ACCEPT:
            self.charging_task_assigned = False

            # clear charging task
            self.clear_charging_task()

            print("[Charging Release] Charge Release..........................")
            self.robot_ros.release_charging()

            # TODO: check connect release???
            # send charge release event
            print("\n>>> [Charge Release] Send Robot Charge Release Event to Server.............")
            charge_release_event_ = self.get_robot_information(ROBOT_EVENT_SUBCODE, UNDOCKING_EVENT)
            self.send_robot_info(charge_release_event_)

            # change to waiting state
            self.work_state = WAITING_TASK

            # charging state
            self.charging_waiting_state = CHARGER_RELEASE

            # send basic information
            self.select_upload_info = BASIC_DATA

            # clear charging data
            self.is_sent_charging_data = False
            self.sent_charging_data = ""

    def check_battery_level_charging(self):
        """
        if battery value >= threshold -> docking release
        """
        if BATTERY_NUM == 2:
            # battery 1
            battery1_ = self.robot_ros.get_battery1_level()

            # battery 2
            battery2_ = self.robot_ros.get_battery2_level()

            if int(battery1_) >= CHARGING_BATTERY_THRESHOLD and int(battery2_) >= CHARGING_BATTERY_THRESHOLD:
                return True

        elif BATTERY_NUM == 1:
            # battery 1
            battery1_ = self.robot_ros.get_battery1_level()

            if int(battery1_) >= CHARGING_BATTERY_THRESHOLD:
                return True

        return False

    def set_jet_strength(self, body_data):
        """
        set jet strength
        """

        ctrl_cmd = body_data["body"]["001"]

        # set jet strength
        self.robot_ros.control_pump(ctrl_cmd)

        # send robot response
        self.send_response_from_robot(info_code=CONTROL_CODE)

        # update injection amount
        self.update_injection_amount(ctrl_cmd)

        print("\n>>> [Pump-Jet Strength] Control Pump ------------------")
        print(">> Jet Strength Value: ", str(ctrl_cmd))

        # write log
        info_ = "value: {}".format(ctrl_cmd)
        log_insert('{}\t{}'.format(SET_PUMP_TASK_LOG, info_), logging.INFO)

    def save_map_image(self, body_data):
        """
        save map image
        """
        if not os.path.isdir(root_dir + "/maps"):
            os.mkdir(root_dir + "/maps")

        # print("Body json: ", body_data["body"]["data"])

        map_data = ''.join(format(ord(x), 'X') for x in body_data["body"]["data"])
        map_id = body_data["body"]["id"]
        map_ver = body_data["body"]["ver"]

        print(">> Save Map ----------------")
        print("Map ID: ", map_id)
        print("Map Version: ", map_ver)

        map_folder_ = map_id
        if not os.path.isdir(root_dir + "/maps/" + map_folder_):
            os.mkdir(root_dir + "/maps/" + map_folder_)

        map_file_path = root_dir + "/maps/" + map_folder_ + "/%s.png" % map_id
        with open(map_file_path, "wb") as map_file:
            map_file.write(base64.decodebytes(bytes.fromhex(map_data)))

        # save map yaml file
        self.save_map_yaml(root_dir + "/maps/" + map_folder_, map_id, body_data["body"]["definition"][0])

        # save map json file
        self.save_map_json(root_dir + "/maps/" + map_folder_, map_id, body_data["body"])

        # update map id and version
        self.update_current_map_id(map_id, map_ver)

        # write log
        info_ = "id:{}/ver:{}".format(map_id, map_ver)
        log_insert('{}\t{}'.format(SAVE_MAP_LOG, map_info_), logging.INFO)

    def save_map_yaml(self, map_path, map_id, map_info):
        """
        create map information in yaml format
        """
        # save yaml file
        yaml_data = ""
        yaml_data += "image: %s.png\n" % str(map_id)
        yaml_data += "resolution: %f\n" % float(map_info["resolution"])
        yaml_data += "origin: [%f, %f, %f]\n" % (float(map_info["origin_x"]), float(map_info["origin_y"]),
                                                float(map_info["origin_yaw"]))
        yaml_data += "negate: %d\n" % int(map_info["negate"])
        yaml_data += "occupied_thresh: %f\n" % float(map_info["occupied_thresh"])
        yaml_data += "free_thresh: %f\n" % float(map_info["free_thresh"])

        # write data
        yaml_file_name = "%s.yaml" % str(map_id)

        with open(map_path + "/" + yaml_file_name, 'w') as yaml_file:
            yaml_file.write(yaml_data)

        yaml_file.close()

    def save_map_json(self, map_path, map_id, map_data):
        """
        create map information in json format
        """

        map_info = map_data["definition"][0]

        data = dict()
        data["origin"] = dict()

        # convert yaw to xyzw
        euler_data = euler_to_quaternion(0, 0, float(map_info["origin_yaw"]))

        data["origin"]["orientation"] = dict()
        data["origin"]["orientation"]["x"] = euler_data[0]
        data["origin"]["orientation"]["y"] = euler_data[1]
        data["origin"]["orientation"]["z"] = euler_data[2]
        data["origin"]["orientation"]["w"] = euler_data[3]

        data["origin"]["position"] = dict()
        data["origin"]["position"]["x"] = float(map_info["origin_x"])
        data["origin"]["position"]["y"] = float(map_info["origin_y"])
        data["origin"]["position"]["z"] = 0.0

        data["resolution"] = float(map_info["resolution"])

        data["id"] = map_data["id"]
        data["ver"] = map_data["ver"]

        pix_map = Image.open(map_path + "/%s.png" % map_id)
        map_width, map_height = pix_map.size

        data["height"] = map_height
        data["width"] = map_width

        # encode data in json format
        map_info_data = json.dumps(data, indent=4, sort_keys=True)

        # write data
        with open(map_path + "/%s.json" % str(map_id), 'w') as json_file:
            json_file.write(map_info_data)

    def save_nodes_data(self, body_data):
        """
            save nodes data in json format
        """
        map_id = body_data["body"]["id"]
        map_folder_ = map_id

        if not os.path.isdir(root_dir + "/maps/" + map_folder_):
            print("[Map-Nodes] Must send map %s before sending nodes data." % map_id)

            # write log
            info_ = "no have {} map folder in directory".format(map_id)
            log_insert('{}\t{}'.format(SAVE_NODE_LOG, info_), logging.ERROR)

            return False

        else:
            print(">> Save Nodes Data ---------------")
            print("Map ID: ", map_id)

            node_data = body_data["body"]

            if len(node_data["nodes"]) > 0:
                for i in range(len(node_data["nodes"])):
                    node_data["nodes"][i]["x"] = float(node_data["nodes"][i]["x"])
                    node_data["nodes"][i]["y"] = float(node_data["nodes"][i]["y"])

            # encode data in json format
            node_data_json = json.dumps(node_data, indent=4, sort_keys=True)

            # write data
            map_path = root_dir + "/maps/" + map_folder_
            with open(map_path + "/%s_nodes.json" % str(map_id), 'w') as json_file:
                json_file.write(node_data_json)

            # write log
            info_ = "saved nodes data in {} folder".format(map_id)
            log_insert('{}\t{}'.format(SAVE_NODE_LOG, info_), logging.INFO)

            return True

    def save_paths_data(self, body_data):
        """
            save paths data in json format
        """
        map_id = body_data["body"]["id"]
        map_folder_ = map_id

        if not os.path.isdir(root_dir + "/maps/" + map_folder_):
            print("[Map-Paths] Must send map %s before sending paths data." % map_id)

            # write log
            info_ = "no have {} map folder in directory".format(map_id)
            log_insert('{}\t{}'.format(SAVE_PATH_LOG, info_), logging.ERROR)

            return False

        else:
            print(">> Save Paths Data ---------------")
            print("Map ID: ", map_id)

            path_data = body_data["body"]

            # encode data in json format
            path_data_json = json.dumps(path_data, indent=4, sort_keys=True)

            # write data
            map_path = root_dir + "/maps/" + map_folder_
            with open(map_path + "/%s_paths.json" % str(map_id), 'w') as json_file:
                json_file.write(path_data_json)

            # write log
            info_ = "saved path data in {} folder".format(map_id)
            log_insert('{}\t{}'.format(SAVE_PATH_LOG, info_), logging.INFO)

            return True

    def send_data(self, robot_data):
        """
        data packing
        :param header_frame:
        :param body_frame:
        :param ksm_encoder:
        :param robot_key:
        :param access_key:
        :param robot_data:
        """
        # body data
        self.body_frame.set_body_data(convert_body_data(robot_data))

        # header data
        self.header_frame.set_robot_key(self.robot_key)
        self.header_frame.set_access_key(self.access_key)
        self.header_frame.set_check_sum(0)
        self.header_frame.set_cmd_code(200)  # when sending robot information, must use command code 200
        self.header_frame.set_create_time(get_create_time())
        self.header_frame.set_error_code(0)
        self.header_frame.set_info_code(0)
        self.header_frame.set_body_length(self.body_frame.get_body_length())

        byte_arr = self.ksm_encoder.make_ksm_data(self.header_frame, self.body_frame, flag_=False)
        # calculate checksum based on CRC32
        self.header_frame.set_check_sum(calc_checksum(byte_arr))
        byte_arr = self.ksm_encoder.make_ksm_data(self.header_frame, self.body_frame, flag_=False)

        return byte_arr

    def send_robot_info(self, robot_data):
        """
        send robot information to server
        """

        # print("\n>>> Send Robot Information to Server------------------")
        print("Send Body Data: ", robot_data)

        # send robot data to server
        # send_bytes_data = bytes.fromhex(self.send_data(robot_data))

        # for resend data when session is not connected
        if (self.select_upload_info == CHARGING_DATA and self.update_robot_info_flag) or self.first_send_charging_data:
            # check send charging data
            self.is_sent_charging_data = True
            self.sent_charging_data = robot_data
            self.first_send_charging_data = False
                    
        connect_session_ = False
        print("Test send............")
        while not connect_session_:
            try:
                print("Test send info in loop....")
                # send data to server
                self.s_obj.sendall(bytes.fromhex(self.send_data(robot_data)))

                connect_session_ = True
                print("\n[Sent Info] Sent Robot Information to Server..............")
                break
            
            # except socket.timeout:
            #    pass 

            except:
                # reconnect to session and request access key
                print(">>> [Send Info Error] Access Key is not valid........Reconnect to session!")
                # write logs
                log_insert('{}\t{}'.format("send_info_error", "reconnect_session"), logging.ERROR)
                self.reconnect_session()
                                
                pass

            # wait 0.1s
            time.sleep(0.1)

    def response_from_robot(self, error_code=0, info_code=0):
        """
        response message
        """

        # body data
        self.body_frame.set_body_data("")

        # header data
        self.header_frame.set_robot_key(self.robot_key)
        self.header_frame.set_access_key(self.access_key)
        self.header_frame.set_check_sum(0)
        self.header_frame.set_cmd_code(301)
        self.header_frame.set_create_time(get_create_time())
        self.header_frame.set_error_code(error_code)
        self.header_frame.set_info_code(info_code)
        self.header_frame.set_body_length(0)

        byte_arr = self.ksm_encoder.make_ksm_data(self.header_frame, self.body_frame, flag_=False)
        # calculate checksum based on CRC32
        self.header_frame.set_check_sum(calc_checksum(byte_arr))
        byte_arr = self.ksm_encoder.make_ksm_data(self.header_frame, self.body_frame, flag_=True)

        return byte_arr

    def send_response_from_robot(self, error_code=0, info_code=0):
        """
        send response from robot
        """

        connect_session_ = False
        while not connect_session_:
            try:
                # send response to server
                self.s_obj.sendall(bytes.fromhex(self.response_from_robot(error_code=error_code, info_code=info_code)))

                connect_session_ = True
                print("\n[Sent Response] Sent Response from Robot..............")

            # except socket.timeout:
            #    pass

            except:
                # reconnect to session and request access key
                print(">>> [Send Response Error] Access Key is not valid........Reconnect to session!")
                # write logs
                log_insert('{}\t{}'.format("send_response_error", "reconnect_session"), logging.ERROR)
                self.reconnect_session()

                pass

    def update_uv_lamp_state(self, uva_state, uvc_state):
        """
        update uv state("00", "01", ...)
        """
        self.uva_state = uva_state
        self.uvc_state = uvc_state

    def update_injection_amount(self, inj_amount):
        """
        update injection amount
        """
        self.inj_amount = inj_amount

    def create_current_working_task(self):
        """
        create current working tasks file
        """

        data = dict()
        data["task_map_id"] = ""
        data["task_map_ver"] = ""
        data["task_assigned"] = "0"
        data["task_id"] = ""
        data["task_from_node"] = ""
        data["task_to_node"] = ""
        
        data["charging_task_assigned"] = "0"
        data["charging_task_id"] = ""
        
        # encode data in json format
        task_info_ = json.dumps(data, indent=4, sort_keys=True)

        if not os.path.isdir(root_dir + "/tasks"):
            os.mkdir(root_dir + "/tasks")

        # write data
        with open(root_dir + "/tasks/tasks_manager.json", 'w') as json_file:
            json_file.write(task_info_)

    def update_current_map_id(self, map_id, map_ver):
        """
        update map id information
        """

        if not os.path.exists(root_dir + "/tasks/tasks_manager.json"):
            self.create_current_working_task()
        
        else:
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
                
                data["task_map_id"] = str(map_id)
                data["task_map_ver"] = str(map_ver)

            # encode data in json format
            data_ = json.dumps(data, indent=4, sort_keys=True)

            # update data
            with open(root_dir + "/tasks/tasks_manager.json", 'w') as json_file:
                json_file.write(data_)

    def update_task_plan(self, task_assigned="0", task_id="", from_node="", to_node=""):
        """
        update task plan
        """

        if not os.path.exists(root_dir + "/tasks/tasks_manager.json"):
            self.create_current_working_task()
        
        else:
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
                
                data["task_assigned"] = str(task_assigned)
                data["task_id"] = str(task_id)
                data["task_from_node"] = str(from_node)
                data["task_to_node"] = str(to_node)

            # encode data in json format
            data_ = json.dumps(data, indent=4, sort_keys=True)

            # update data
            with open(root_dir + "/tasks/tasks_manager.json", 'w') as json_file:
                json_file.write(data_)

    def update_charging_task_plan(self, charging_task_assigned="0", charging_task_id=""):
        """
        update charging task plan
        """

        if not os.path.exists(root_dir + "/tasks/tasks_manager.json"):
            self.create_current_working_task()
        
        else:
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
                
                data["charging_task_assigned"] = str(charging_task_assigned)
                data["charging_task_id"] = str(charging_task_id)
               
            # encode data in json format
            data_ = json.dumps(data, indent=4, sort_keys=True)

            # update data
            with open(root_dir + "/tasks/tasks_manager.json", 'w') as json_file:
                json_file.write(data_)

    def clear_moving_task(self):
        """
        clear moving task plan
        """

        if not os.path.exists(root_dir + "/tasks/tasks_manager.json"):
            self.create_current_working_task()
        
        else:
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
                
                data["task_assigned"] = "0"
                data["task_id"] = ""
                data["task_from_node"] = ""
                data["task_to_node"] = ""

            # encode data in json format
            data_ = json.dumps(data, indent=4, sort_keys=True)

            # update data
            with open(root_dir + "/tasks/tasks_manager.json", 'w') as json_file:
                json_file.write(data_)

    def clear_charging_task(self):
        """
        clear charging task plan
        """

        if not os.path.exists(root_dir + "/tasks/tasks_manager.json"):
            self.create_current_working_task()
        
        else:
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
            
                data["charging_task_assigned"] = "0"
                data["charging_task_id"] = ""

            # encode data in json format
            data_ = json.dumps(data, indent=4, sort_keys=True)

            # update data
            with open(root_dir + "/tasks/tasks_manager.json", 'w') as json_file:
                json_file.write(data_)

    def clear_task_plan(self):
        """
        clear task plan
        """

        if not os.path.exists(root_dir + "/tasks/tasks_manager.json"):
            self.create_current_working_task()
        
        else:
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
                
                data["task_assigned"] = "0"
                data["task_id"] = ""
                data["task_from_node"] = ""
                data["task_to_node"] = ""

                data["charging_task_assigned"] = "0"
                data["charging_task_id"] = ""

            # encode data in json format
            data_ = json.dumps(data, indent=4, sort_keys=True)

            # update data
            with open(root_dir + "/tasks/tasks_manager.json", 'w') as json_file:
                json_file.write(data_)

    def check_current_task(self):
        """
        check current task
        """

        # get task status
        task_status_ = self.read_task_plan()
        task_assigned_ = task_status_["task_assigned"]
        charging_task_assigned_ = task_status_["charging_task_assigned"]
        
        if (self.task_assigned is True) and (task_assigned_ == "1"):
            return TASK_ASSIGNED
        
        if (self.charging_task_assigned is True) and (charging_task_assigned_ == "1"):
            return CHARGING_TASK_ASSIGNED
        
        return NO_TASK

    def task_planning(self, task_type):
        """
        charging task
        """

        if self.check_current_task() == TASK_ASSIGNED:
            # reject new task if any task was assigned in robot
            if task_type == QUARANTINE_TASK:
                print("[REJECT TASK] a task is running.............")
                return TASK_REJECT
            
            # accept charging task
            if task_type == CHARGING_TASK:
                print("[CHARGE TASK STACK] stack charging task...........")
                return TASK_STACKING
            
            # accept charging release task
            if task_type == CHARGING_RELEASE_TASK:
                print("[REJECT RELEASE TASK] a task is running.............")
                return TASK_REJECT

        elif self.check_current_task() == CHARGING_TASK_ASSIGNED:
            # reject new task if any task was assigned in robot
            if task_type == QUARANTINE_TASK:
                print("[REJECT MOVING TASK] a charging task is running.............")
                return TASK_REJECT
            
            # reject charging task
            if task_type == CHARGING_TASK:
                print("[REJECT CHARGING TASK] a charging task is running.............")
                return TASK_REJECT
            
            # accept charging release task
            if task_type == CHARGING_RELEASE_TASK:
                print("[ACCEPT RELEASE TASK] release charging task.............")
                return TASK_ACCEPT
        
        elif self.check_current_task() == NO_TASK:
            # accept charging release task
            if task_type == CHARGING_RELEASE_TASK:
                print("[REJECT RELEASE TASK] no task is running.............")
                return TASK_REJECT

            return TASK_ACCEPT

    def read_task_plan(self):
        """
        read current task plan
        """

        if os.path.exists(root_dir + "/tasks/tasks_manager.json"):
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
            
            return data
        
        else:
            self.create_current_working_task()
            with open(root_dir + "/tasks/tasks_manager.json") as json_file:
                data = json.load(json_file)
            
            return data

    def connect_session(self):
        """
        connect to session
        """

        # create an INET, STREAMing socket
        self.s_obj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            # connect to the server
            print("Connect to server: %s : %d" % (HOST, PORT))
            self.s_obj.connect((HOST, PORT))

            print("Server connection successful.")

        except socket.error:
            print("[Error] Failed to connect to server.")

            return False
        
        else:
            return True

    def disconnect_session(self):
        """
        disconnect to session
        """

        print("Disconnect to session.")
        self.s_obj.close()
                    
    def recv_msg_data(self, msg, msg_data, header_frame, body_frame, ksm_decoder):
        """
        receive message data
        """
        recv_timer = threading.Timer(0.01, self.recv_msg_data, [msg, msg_data, header_frame, body_frame, ksm_decoder])
        recv_timer.start()

        if self.flag_:
            self.flag_ = False

            while True:
                try:
                    print(">>> Listen to data......")
                    # receive response from the server
                    msg = self.s_obj.recv(RECV_MAX_BUF_SIZE)

                except socket.timeout:
                    print("Socket time out!")

                    pass
                
                except socket.error:
                    # reconnect to session and request access key
                    print(">>> [Recv Data Error] Access Key is not valid........Reconnect to session!")
                    # write logs
                    log_insert('{}\t{}'.format("recv_data_error", "reconnect_session"), logging.ERROR)
                    self.reconnect_session()
      
                    break

                if len(msg) > 0:
                    # receive all data on socket
                    msg_data = msg_data + msg

                    # check received body data len
                    header_msg_ = ksm_decoder.make_ksm_header_frame(msg_data, header_frame)
                    body_msg_ = ksm_decoder.make_ksm_body_frame(msg_data, body_frame)

                    # body data len in header
                    body_len = header_msg_.get_body_length_value()

                    # received body data len
                    body_data = body_msg_.get_body_value()

                    if body_len == len(body_data) and body_len != 0:
                        # print("Received CMD MSG len: ", len(msg_data))

                        header_msg = self.ksm_decoder.make_ksm_header_frame(msg_data, self.header_frame)
                        body_msg = self.ksm_decoder.make_ksm_body_frame(msg_data, self.body_frame)

                        # check received data checksum
                        if compare_checksum(header_msg, body_msg):
                            if get_received_msg(header_msg) == RECV_SUCCESS:
                                print("Received a command from server. ")
                                print("Received body data: ", body_msg.get_body_value())

                                # server command analysis
                                self.server_message_process()

                                msg = b''
                                msg_data = b''

                                break

                            elif get_received_msg(header_msg) == NO_ACCESS_KEY:
                                msg = b''
                                msg_data = b''

                                print("[Error Cmd] Reconnect to session!")
                                # reconnect to session and request access key
                                print(">>> [Send Info] Access Key is not valid........Reconnect to session!")
                                # write logs
                                log_insert('{}\t{}'.format("recv_data_no_access_key", "reconnect_session"), logging.ERROR)
                                self.reconnect_session()
                                                                                    
                                break

                            else:
                                msg = b''
                                msg_data = b''
                                print("[Error] Can not receive command msg from server.")

                                break

                            # if body_len != 0:
                            #     self.update_robot_info_flag = True

                    if body_len == 0:

                        header_msg = self.ksm_decoder.make_ksm_header_frame(msg_data, self.header_frame)
                        body_msg = self.ksm_decoder.make_ksm_body_frame(msg_data, self.body_frame)

                        # check received data checksum
                        if compare_checksum(header_msg, body_msg):
                            if get_received_msg(header_msg) == RECV_SUCCESS:
                                print("Received a response from server. ")

                                msg = b''
                                msg_data = b''

                                break

                            elif get_received_msg(header_msg) == NO_ACCESS_KEY:
                                msg = b''
                                msg_data = b''
                                print("[Error Response] Reconnect to session!")
                                
                                # reconnect to session and request access key
                                print(">>> [Send Response] Access Key is not valid........Reconnect to session!")
                                # write logs
                                log_insert('{}\t{}'.format("recv_response_no_access_key", "reconnect_session"), logging.ERROR)
                                self.reconnect_session()
                
                                break

                            else:
                                msg = b''
                                msg_data = b''
                                print("[Error] Can not receive response msg from server.")
                                break

                        msg = b''
                        msg_data = b''

            self.flag_ = True

        if self.timer_cancel:
            if recv_timer is not None:
                print("Recv Data Cancel ..............")
                recv_timer.cancel()   
               
                
if __name__ == '__main__':

    header_frame = HeaderFrame()
    body_frame = BodyFrame()

    ksm_encoder = KSMEncoder()
    ksm_decoder = KSMDecoder()

    msg = b''
    msg_data = b''

    # task manager
    task_ = TaskManager()

    try:
        # request access key
        task_.communication_setting(header_frame, body_frame, ksm_encoder, ksm_decoder, ROBOT_KEY)

        task_.set_update_robot_info_flag(True)
        task_.update_robot_basic_info()
        task_.update_robot_charging_info()
        task_.task_process_manager()
        task_.check_robot_working_state()

        # receive msg flag
        task_.flag_ = True
        # receive msg from server
        task_.recv_msg_data(msg, msg_data, header_frame, body_frame, ksm_decoder)
   
    except KeyboardInterrupt:
        # close the connection
        task_.disconnect_session()

        # update robot basic information and charging information
        task_.set_update_robot_info_flag(False)

        # cancel all timers
        task_.cancel_timer(True)

        print("Close program...")
